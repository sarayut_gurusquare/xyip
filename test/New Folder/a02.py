import cv2
import os
import numpy as np

date_time_height                                = 70
resize_ratio1                                   = 0.5
cwd                                             = os.getcwd()
input_folder_path                               = os.path.join(cwd,'a01_in')
files                                           = os.listdir(input_folder_path)
i=5
img                                             = cv2.imread(os.path.join(input_folder_path,files[i]))
n=30
kernel1                                         = np.ones((1,40),np.uint8)
kernelx                                         = np.ones((1,40),np.uint8)

im1                                         = img
height1, width1 , ch                        = im1.shape
width2                                      = int(width1  * resize_ratio1)
height2                                     = int(height1 * resize_ratio1)
im2                                         = cv2.resize(im1, (width2,height2)) 
# Convert image to gray scale
im3                                         = im2[date_time_height:height2,0:width2]
im4                                         = cv2.cvtColor(im3, cv2.COLOR_BGR2GRAY)
im_blur                                     = cv2.GaussianBlur(im4, (7,7), 0)
im_white_tophat                             = cv2.morphologyEx(im_blur, cv2.MORPH_TOPHAT, kernel1)
im_black_tophat                             = cv2.morphologyEx(im_blur, cv2.MORPH_BLACKHAT, kernel1)


im_add                                      = cv2.add(im_white_tophat,im_black_tophat) 
im_bi                                       = cv2.threshold(im_add,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]

im_close                                    = cv2.morphologyEx(im_bi, cv2.MORPH_CLOSE, kernelx)


cv2.imshow('im_blur',im_blur)
cv2.imshow('im_w',im_white_tophat)
cv2.imshow('im_b',im_black_tophat)
cv2.imshow('im_add',im_add)
cv2.imshow('im_bi',im_bi)
cv2.imshow('im_close',im_close)

cv2.waitKey(0)
cv2.destroyAllWindows()