import os
import numpy as np
import cv2
import glob
import datetime
import shutil
import re
import sys
#import post_processing

crop_expand_size_pixels = 10
full_width = 800
full_height = 600
min_ratio = 2.5
max_ratio = 3.5

#config 
date_time_height                                = 70
crop_height_ratio                               = 0.6
crop_width_ratio                                = 0.6
resize_ratio1                                   = 0.6
resize_ratio2                                   = 0.7
max_area_contour                                = 50000
min_area_contour                                = 10000
kernel1                                         = np.ones((1,60),np.uint8)
kernel2                                         = np.ones((15,15),np.uint8)
min_ratio_rect                                  = 2
max_ratio_rect                                  = 10
buffer1                                         = 20
small_area                                      = 150
buf                                             = 10
tran_y                                          = 40
tran_x                                          = tran_y*10

cwd                                             = os.getcwd()
input_folder_path                               = os.path.join(cwd,'a01_in')
files                                           = os.listdir(input_folder_path)
i=0
img                                         = cv2.imread(os.path.join(input_folder_path,files[i])) 


# Resize with resize_ratio1
im1                                         = img
height1, width1 , ch                        = im1.shape
width2                                      = int(width1  * resize_ratio1)
height2                                     = int(height1 * resize_ratio1)
im2                                         = cv2.resize(im1, (width2,height2)) 


# Convert image to gray scale
im3                                         = im2[date_time_height:height2,0:width2]
im4                                         = cv2.cvtColor(im3, cv2.COLOR_BGR2GRAY)
im6                                         = cv2.GaussianBlur(im4, (7,7), 0)
cv2.imshow('ori',im6)
# =============================================================================

 
 # ???
height3, width3                             = im6.shape
height4                                     = int(height3 * crop_height_ratio)
width4                                      = int(width3  * crop_width_ratio)
im8                                         = im6[0:height4,0:width4]

im7                                         = cv2.threshold(im6, 100, 255, cv2.THRESH_BINARY)[1]
im9                                         = cv2.morphologyEx(im7, cv2.MORPH_CLOSE, kernel1)
cv2.imshow('imnorm',im9)
# =============================================================================
im_white_tophat                             = cv2.morphologyEx(im6, cv2.MORPH_TOPHAT, kernel2)
im_black_tophat                             = cv2.morphologyEx(im6, cv2.MORPH_BLACKHAT, kernel2)
im_add                                      = cv2.add(im_white_tophat,im_black_tophat) 
im_bi                                       = cv2.threshold(im_add,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]
im_close                                    = cv2.morphologyEx(im_bi, cv2.MORPH_CLOSE, kernel1)
cv2.imshow('imhat',im_close)
# =============================================================================
cts1                                        = cv2.findContours(im9, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[1]
for j in xrange(len(cts1)): # Run every contours
    area                                    = cv2.contourArea(cts1[j])

    # Check area of contour
    if area < min_area_contour or area > max_area_contour :
        continue

    # # Check contour position that should not be adjacented the corners.
    x,y,w,h                                 = cv2.boundingRect(cts1[j])
    if x == 0 or y == 0 or x+w >= width4 or y+h >= height4 :
        continue

    # Check ratio of the rectangle
    ratio_rect                              = w / float(h)
    if ratio_rect > max_ratio_rect or ratio_rect < min_ratio_rect :
        continue
     
    x1                                      = x - buffer1
    x2                                      = x + w + buffer1
    y1                                      = y - buffer1
    y2                                      = y + h + buffer1
    if x1 < 0 or y1 < 0 or x2 > width4 or y2 > height4 :
        continue

    # Crop image grayscale to do perspective transformation
    im10                                    = im8[y1:y2,x1:x2]



#    cv2.imshow('im10from7',im7[y1:y2,x1:x2])
#    cv2.imshow('im10from8',im8[y1:y2,x1:x2])



cv2.waitKey(0)
cv2.destroyAllWindows()