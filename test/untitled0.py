import cv2
import numpy as np

x=235
y=51

im1             = cv2.imread('im13.png',1)
cv2.imshow('im1',im1)
im2 = np.zeros(im1.shape,dtype=np.uint8)
#cv2.imshow('im2',im2)
pts1            = np.float32([[166,84],[184,88],[153,132],[171,137]])
pts2            = np.float32([[100,0],[150,0],[100,100],[150,100]])
M               = cv2.getPerspectiveTransform(pts1,pts2)
dst             = cv2.warpPerspective(im1,M,(495,234))
cv2.imshow('dst',dst)
          
cv2.waitKey(0)
cv2.destroyAllWindows()