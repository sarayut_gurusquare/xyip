import os, cv2, numpy as np

def f_most_pos(order_number_of_contour,all_contours):
    cnt=all_contours[order_number_of_contour]
    leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
    rightmost = tuple(cnt[cnt[:,:,0].argmax()][0])
    topmost = tuple(cnt[cnt[:,:,1].argmin()][0])
    bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])
    return leftmost, rightmost, topmost, bottommost

small_area          = 150
buf                 = 5
cwd                 = os.getcwd()
path_input_folder   = os.path.join(cwd,'pic crop')
path_output_folder  = os.path.join(cwd,'output')
files               = os.listdir(path_input_folder)

#for i in xrange(len(files)):
i=0
if files[i].endswith('.png'):
    path_pic        = os.path.join(path_input_folder,files[i])
    pic0            = cv2.imread(path_pic)
    pic1            = cv2.cvtColor(pic0,cv2.COLOR_BGR2GRAY)
    pic2            = cv2.GaussianBlur(pic1,(5,5),0)
    
    ret3,pic3       = cv2.threshold(pic2,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    ###
#    cv2.imshow('ori_'+files[i],pic1)
    cv2.imshow('pic2',pic0)
#    cv2.imshow('pic3',pic0)
    ###

    _, ctrs, hrc    = cv2.findContours(pic3.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    a_pos           = []
    a_ctr           = []
    d_pos           = {}
    for j in xrange(len(ctrs)):
        ctr_area    = cv2.contourArea(ctrs[j])
        if ctr_area < small_area :
            continue
        if hrc[0][j][3] > -1 :
            continue
        
        rect        = cv2.minAreaRect(ctrs[j])
        box1        = cv2.boxPoints(rect)
        box2        = np.int0(box1)
        ###
        pic4        = cv2.cvtColor(pic1.copy(),cv2.COLOR_GRAY2BGR)
        pic5        = cv2.drawContours(pic4,[box2],0,(0,0,255),2)
#        cv2.imshow(str(j),pic5)    
        

        ###
        a_ctr_pos   = []
        for k in box2:
            a_pos.append(k)
            a_ctr.append(j)
            a_ctr_pos.append(k)
        d_pos[j]    = a_ctr_pos
            
    npa_pos         = np.asarray(a_pos, dtype=np.int64)
    npa_ctr         = np.asarray(a_ctr, dtype=np.int64)
    npa_ctr.shape   =(npa_pos.shape[0],1)
    npa_pos_ctr     = np.append(npa_pos,npa_ctr,axis=1)
    
    npa_pos_x       = npa_pos_ctr[:,0]
    npa_pos_y       = npa_pos_ctr[:,1]
    npa_pos_x_sort  = np.argsort(npa_pos_x)
    npa_pos_y_sort  = np.argsort(npa_pos_y)
    pos_leftmost    = npa_pos_ctr[npa_pos_x_sort[0]][0:2]
    pos_rightmost   = npa_pos_ctr[npa_pos_x_sort[-1]][0:2]
    pos_topmost     = npa_pos_ctr[npa_pos_y_sort[0]][0:2]
    pos_bottommost  = npa_pos_ctr[npa_pos_y_sort[-1]][0:2] 
    
    ctr_leftmost    = npa_pos_ctr[npa_pos_x_sort[0]][2]
    ctr_rightmost   = npa_pos_ctr[npa_pos_x_sort[-1]][2]
    
    
    img = pic0
    img = cv2.circle(pic0,(pos_leftmost[0],pos_leftmost[1]), 2, (0,0,255), -1)
    img = cv2.circle(pic0,(pos_rightmost[0],pos_rightmost[1]), 2, (0,0,255), -1)
    img = cv2.circle(pic0,(pos_topmost[0],pos_topmost[1]), 2, (0,0,255), -1)
    img = cv2.circle(pic0,(pos_bottommost[0],pos_bottommost[1]), 2, (0,0,255), -1)
    cv2.imshow('0',img)
cv2.waitKey(0)
cv2.destroyAllWindows()    
    
    
    
    
    
    
    
#    img = pic0
##    img = cv2.circle(pic0,(pos_topleft[0],pos_topleft[1]), 2, (0,0,255), -1)
#    img = cv2.circle(pic0,(pos_bottomleft[0],pos_bottomleft[1]), 2, (0,0,255), -1)
##    img = cv2.circle(pic0,(pos_topright[0],pos_topright[1]), 2, (0,0,255), -1)
##    img = cv2.circle(pic0,(pos_bottomright[0],pos_bottomright[1]), 2, (0,0,255), -1)
#    cv2.imshow('0',img)
cv2.waitKey(0)
cv2.destroyAllWindows() 
#%%
    
   

    #FIND 4 POSITION
    ctr_left            = d_pos[ctr_leftmost]
    ctr_right           = d_pos[ctr_rightmost]
    
    n_ctr_left          = np.asarray(ctr_left, dtype=np.int64)
    n_ctr_right         = np.asarray(ctr_right,dtype=np.int64)
    
    n_ctr_left_sort     = np.argsort(n_ctr_left[:,0])
    n_ctr_right_sort    = np.argsort(n_ctr_right[:,0])
    
    left1               = n_ctr_left[n_ctr_left_sort[0]]
    left2               = n_ctr_left[n_ctr_left_sort[1]]
    
    right1              = n_ctr_right[n_ctr_right_sort[-1]]
    right2              = n_ctr_right[n_ctr_right_sort[-2]]
    
    if left1[1]<left2[1] :
        pos_topleft     = left1
        pos_bottomleft  = left2
    else :
        pos_topleft     = left2 
        pos_bottomleft  = left1
    
    if right1[1]<right2[1] :
        pos_topright    = right1
        pos_bottomright = right2
    else :
        pos_topright    = right2
        pos_bottomright = right1       
    

    
    
    #BUFFER
    buf_pos_topleft         = pos_topleft
    buf_pos_bottomleft      = pos_bottomleft
    buf_pos_topright        = pos_topright
    buf_pos_bottomright     = pos_bottomright
    
    buf_pos_topleft[0]      = buf_pos_topleft[0]-buf
    buf_pos_topleft[1]      = buf_pos_topleft[1]-buf
    buf_pos_topright[0]     = buf_pos_topright[0]+buf
    buf_pos_topright[1]     = buf_pos_topright[1]-buf
    buf_pos_bottomleft[0]   = buf_pos_bottomleft[0]-buf
    buf_pos_bottomleft[1]   = buf_pos_bottomleft[1]+buf
    buf_pos_bottomright[0]  = buf_pos_bottomright[0]+buf
    buf_pos_bottomright[1]  = buf_pos_bottomright[1]+buf
      
    pos_topmost[0]      = pos_topmost[0]-buf
    pos_topmost[1]      = pos_topmost[1]-buf
    pos_rightmost[0]    = pos_rightmost[0]+buf
    pos_rightmost[1]    = pos_rightmost[1]-buf
    pos_leftmost[0]     = pos_leftmost[0]-buf
    pos_leftmost[1]     = pos_leftmost[1]+buf
    pos_bottommost[0]   = pos_bottommost[0]+buf
    pos_bottommost[1]   = pos_bottommost[1]+buf
    
    pts1            = np.float32([buf_pos_topleft,buf_pos_topright,
                                  buf_pos_bottomleft,buf_pos_bottomright]) 
    ###
    pic6            = cv2.cvtColor(pic1.copy(),cv2.COLOR_GRAY2BGR)
    pic7            = cv2.drawContours(pic6,[pts1],0,(0,0,255),2)
#        cv2.imshow(str(j),pic7)
    cv2.imshow('xx',pic6)
    ###
    
    y=80
    x=y*10
    pts2            = np.float32([[0,0],[x,0],[0,y],[x,y]])
    M               = cv2.getPerspectiveTransform(pts1,pts2)
    dst             = cv2.warpPerspective(pic0,M,(x,y))
    ###
#    cv2.imshow('dst_'+files[i],dst)
#        cv2.imwrite(os.path.join(path_output_folder,files[i]),dst)
    ###
cv2.waitKey(0)
cv2.destroyAllWindows()    
