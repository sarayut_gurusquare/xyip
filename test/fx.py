import datetime
import cv2
import os
import numpy as np

# Config to resize image
full_width = 800
full_height = 600

# Capture image ratio
min_ratio = 2.5
max_ratio = 3.5

crop_expand_size_pixels = 10
temp_save_pic_path = os.getcwd()
main_path = os.getcwd()

# For Perspective Transformation
small_area = 150
buf = 10


def __container_no_detection(img):
#    now_datetime = datetime.datetime.now()
#    start_detect_date_time = now_datetime.strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
#    print_detect_date_time = now_datetime.strftime("%Y-%m-%d %H:%M:%S:%f")[:-3]
#
#    print 'Start container detect : ' + start_detect_date_time
    # TODO : Clean files every time
    # clean_files_in_folder(temp_save_pic_path)

    # img = cv2.resize(img, (full_width, full_height))

    img2gray_ori = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img2gray = cv2.medianBlur(img2gray_ori, 5)
    img2gray = cv2.GaussianBlur(img2gray, (7,7), 0)
#    cv2.imshow('img2gray', img2gray)
#    cv2.waitKey()

    ret_write, mask = cv2.threshold(img2gray, 200, 255, cv2.THRESH_BINARY)
#    cv2.imshow('mask', mask)
#    cv2.waitKey()

    image_final = cv2.bitwise_and(img2gray, img2gray, mask=mask)
#    cv2.imshow('First Bitwise_and', image_final)
#    cv2.waitKey()

    ret_write, binarized_img_white_ori = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)  # for black text , cv.THRESH_BINARY_INV
    ret_black, binarized_img_black_ori = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY_INV)

#    combined_binarized_img = cv2.bitwise_or(binarized_img_white_ori, binarized_img_black_ori)

    # print 'Finish Binarize : ' + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
    binarized_img_white = binarized_img_white_ori
    cv2.imshow('First Binarization', binarized_img_white)
#    cv2.waitKey()

    # 09/10/2017 Crop the left panel
    # x from top->down
    # y from left->right

    x_position = 0  # height_position
    height = full_height - x_position
    y_position = 100  # width_position
    width = full_width * 3 / 4
    

    # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
    binarized_img_white = binarized_img_white[y_position:height, x_position:width]


    cv2.imshow('Cropx', binarized_img_white)
    # cv2.waitKey()

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
    binarized_img_white = cv2.dilate(binarized_img_white, kernel, iterations=9)  # dilate , more the iteration more the dilation

#    cv2.imshow('Dilate', binarized_img_white)
    # cv2.waitKey()

    # print 'Crop & Dilate : ' + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]

    image, contours, hier = cv2.findContours(binarized_img_white, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
#    str_datetime = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
#
#    counter = 1
    
    
    # TYPE 1 : Save images each contours and check one by one with tesseract
    count = 0
    for cnt in contours:
        count = count + 1
        # cv2.drawContours(img, [cnt], 0, (0, 255, 0), 2)
        [x, y, w, h] = cv2.boundingRect(cnt)
        x += x_position
        y += y_position
        # print 'Check Coutour Size %', (w * h)
        # print 'x = ' + str(x)
        # print 'y = ' + str(y)
        # print 'w = ' + str(w)
        # print 'h = ' + str(h)

        # filter contours that is adjacent to the edge of image and size (w * h) is less than 40,000 pixels^2 with w > h
        # and ratio h/w
        ratio = w / float(h)
        
#        cv2.rectangle(img.copy(), (x, y), (x + w, y + h), (255, 0, 255), 2)
#        cv2.imshow('Add Contours'+str(count), img)
#        cv2.waitKey()

        if(x > 0 and x + w < full_width and y > 0 and y + h < full_height and w > h and w * h > 40000
           and (ratio >= min_ratio) and (ratio <= max_ratio)):
            # TODO : Call container_no_recognition(img) and write result to text file
            # Write crop images
            # print 'x = ' + str(x)
            # print 'y = ' + str(y)
            # print 'w = ' + str(w)
            # print 'h = ' + str(h)

            # Save contour of the image to tmp path
            # crop_img = img[y:y+h, x:x+w]

            # Expand size
            # crop_img = img[y - crop_expand_size_pixels :y + h + crop_expand_size_pixels, x - crop_expand_size_pixels :x + w + crop_expand_size_pixels]
            # # save image from Binarized image
            # crop_img = binarized_img[y:y+h, x:x+w]

            # Use binarized image to do container no recognition
            crop_img = binarized_img_white_ori[y - crop_expand_size_pixels:y + h + crop_expand_size_pixels,
                       x - crop_expand_size_pixels:x + w + crop_expand_size_pixels]
            
            
            ###
            
            cv2.imshow('xxx',crop_img)
#            cv2.waitKey(0)
#            cv2.destroyAllWindows()
            ###
            
            # TODO
            # Recheck and perspective transformation before sending to Tesseract
            crop_img = doPerspectiveTransformation(crop_img)
            # cv2.imshow('combined image', combined_binarized_img)
            # cv2.waitKey()



            cv2.imshow('Crop', crop_img)
            # cv2.waitKey()
            #####
#            ocr_container_no_result_before = __container_no_recognition(crop_img)
            #####
            #my add
            return crop_img
#            return ocr_container_no_result_before
            #####
# =============================================================================
#             ocr_container_no_result_final = post_processing.post_processing(ocr_container_no_result_before)
#             print 'Detected Container No = ' + ocr_container_no_result_before + ' => ' + ocr_container_no_result_final
# 
#             # if (not(ocr_container_no_result_final == '') and len(ocr_container_no_result_final) >= minimum_length_result and match_container_no_format(ocr_container_no_result_final)):
#             if not (ocr_container_no_result_final == ''):
#                 # print 'Add contour'
#                 result_image_path = os.path.join(result_pic_container_no_path, str_datetime + '-' + str(counter) + '.jpg')
#                 print 'Save image to : ' + result_image_path
#                 cv2.imwrite(result_image_path, crop_img)
# 
#                 # result_text_path = os.path.join(result_ocr_container_no_path, result_ocr_container_no_file)
#                 result_text_path = os.path.join(result_ocr_container_no_path, str_datetime + '-' + str(counter) + '.txt')
#                 print 'Save result to : ' + result_text_path
#                 # with open(os.path.join(result_ocr_container_no_path, str_datetime + '-' + str(counter) + '.txt'), "w") as text_file:
#                 #     text_file.write(ocr_container_no_result_final)
#                 with open(result_text_path, 'a') as text_file: # a is append mode
#                     text_file.write(ocr_container_no_result_final + "\t" + result_image_path + "\t" + print_detect_date_time + '\n')
# 
#                 result_text_path_concat = os.path.join(result_ocr_container_no_path, 'result_concat.txt')
#                 with open(result_text_path_concat, 'a') as text_file:  # a is append mode
#                     text_file.write(ocr_container_no_result_final + "\t" + result_image_path + "\t" + print_detect_date_time + '\n')
# 
#                 counter += 1
# 
#                 cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)
# 
#             # cv2.imshow('Add Contours', img)
#             # cv2.waitKey()
# 
#         # else:
#             # print "Skip contour"
# 
#     return img
# =============================================================================

def __container_no_recognition(crop_image):
    print "Start Recognize : " + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
    result = -1
    # Get Contour and save image
    # cv2.imwrite(os.path.join(temp_save_pic_path, 'temp.jpg'), crop_image)
    # crop_image = cv2.resize(crop_image, (1920,640))
    cv2.imwrite(os.path.join(temp_save_pic_path, 'temp.png'), crop_image, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
    # Call Tesseract
    print 'Tesseract result will be save at : ' + os.path.join(temp_save_pic_path, 'result')
    command = os.path.join(main_path, 'Tesseract-OCR','tesseract.exe') + ' ' + os.path.join(temp_save_pic_path, 'temp.png') + ' ' \
              + os.path.join(temp_save_pic_path, 'result') + ' --psm 13 --oem 0'

    # cv2.imwrite(os.path.join(temp_save_pic_path, 'temp.bmp'), crop_image)
    # # Call Tesseract
    # print 'Tesseract result will be save at : ' + os.path.join(temp_save_pic_path, 'result')
    # command = os.path.join(main_path, 'Tesseract-OCR','tesseract.exe') + ' ' + os.path.join(temp_save_pic_path, 'temp.bmp') + ' ' \
    #           + os.path.join(temp_save_pic_path, 'result') + ' --psm 13 --oem1'



    # print command
    # print os.system(command)

    # Execute Tesseract
    os.system(command)

    # Read result
    with open(os.path.join(temp_save_pic_path, 'result.txt'), 'r') as read_file:
        result = read_file.read()

    print "End Recognize : " + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
    # Return result
    return str(result).strip()

def doPerspectiveTransformation(expect_container_pic):
    _, ctrs, hrc = cv2.findContours(expect_container_pic.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    a_pos = []
    a_ctr = []
    d_pos = {}

    for j in xrange(len(ctrs)):
#        ctr_area = cv2.contourArea(ctrs[j])

        # Check contour area is smaller than small_area then no use it
#        if ctr_area < small_area:
#            continue

        # Draw Min Rect box
        rect = cv2.minAreaRect(ctrs[j])
        box1 = cv2.boxPoints(rect)
        box2 = np.int0(box1)
        ###
        #            pic4        = cv2.cvtColor(pic1.copy(),cv2.COLOR_GRAY2BGR)
        #            pic5        = cv2.drawContours(pic4,[box2],0,(0,0,255),2)
        #            cv2.imshow(str(j),pic5)
        ###

        # TODO : Ask P'Gus that what is it doing here????
        a_ctr_pos = []
        for k in box2:
            a_pos.append(k)
            a_ctr.append(j)
            a_ctr_pos.append(k)
        d_pos[j] = a_ctr_pos

    # NumPy array
    npa_pos = np.asarray(a_pos, dtype=np.int64)
    npa_ctr = np.asarray(a_ctr, dtype=np.int64)
    npa_ctr.shape = (npa_pos.shape[0], 1)
    npa_pos_ctr = np.append(npa_pos, npa_ctr, axis=1)

    npa_pos_x = npa_pos_ctr[:, 0]
    npa_pos_y = npa_pos_ctr[:, 1]
    npa_pos_x_sort = np.argsort(npa_pos_x)
    npa_pos_y_sort = np.argsort(npa_pos_y)
    pos_leftmost = npa_pos_ctr[npa_pos_x_sort[0]][0:2]
    pos_rightmost = npa_pos_ctr[npa_pos_x_sort[-1]][0:2]
    pos_topmost = npa_pos_ctr[npa_pos_y_sort[0]][0:2]
    pos_bottommost = npa_pos_ctr[npa_pos_y_sort[-1]][0:2]

    ctr_leftmost = npa_pos_ctr[npa_pos_x_sort[0]][2]
    ctr_rightmost = npa_pos_ctr[npa_pos_x_sort[-1]][2]

    # FIND 4 POSITION
    ctr_left = d_pos[ctr_leftmost]
    ctr_right = d_pos[ctr_rightmost]

    n_ctr_left = np.asarray(ctr_left, dtype=np.int64)
    n_ctr_right = np.asarray(ctr_right, dtype=np.int64)

    n_ctr_left_sort = np.argsort(n_ctr_left[:, 0])
    n_ctr_right_sort = np.argsort(n_ctr_right[:, 0])

    left1 = n_ctr_left[n_ctr_left_sort[0]]
    left2 = n_ctr_left[n_ctr_left_sort[1]]

    right1 = n_ctr_right[n_ctr_right_sort[-1]]
    right2 = n_ctr_right[n_ctr_right_sort[-2]]

    if left1[1] < left2[1]:
        pos_topleft = left1
        pos_bottomleft = left2
    else:
        pos_topleft = left2
        pos_bottomleft = left1

    if right1[1] < right2[1]:
        pos_topright = right1
        pos_bottomright = right2
    else:
        pos_topright = right2
        pos_bottomright = right1

        # BUFFER
    buf_pos_topleft = pos_topleft
    buf_pos_bottomleft = pos_bottomleft
    buf_pos_topright = pos_topright
    buf_pos_bottomright = pos_bottomright

    buf_pos_topleft[0] = buf_pos_topleft[0] - buf
    buf_pos_topleft[1] = buf_pos_topleft[1] - buf
    buf_pos_topright[0] = buf_pos_topright[0] + buf
    buf_pos_topright[1] = buf_pos_topright[1] - buf
    buf_pos_bottomleft[0] = buf_pos_bottomleft[0] - buf
    buf_pos_bottomleft[1] = buf_pos_bottomleft[1] + buf
    buf_pos_bottomright[0] = buf_pos_bottomright[0] + buf
    buf_pos_bottomright[1] = buf_pos_bottomright[1] + buf

    pos_topmost[0] = pos_topmost[0] - buf
    pos_topmost[1] = pos_topmost[1] - buf
    pos_rightmost[0] = pos_rightmost[0] + buf
    pos_rightmost[1] = pos_rightmost[1] - buf
    pos_leftmost[0] = pos_leftmost[0] - buf
    pos_leftmost[1] = pos_leftmost[1] + buf
    pos_bottommost[0] = pos_bottommost[0] + buf
    pos_bottommost[1] = pos_bottommost[1] + buf

    pts1 = np.float32([buf_pos_topleft, buf_pos_topright,
                       buf_pos_bottomleft, buf_pos_bottomright])
    ###
    #    pic6            = cv2.cvtColor(pic1.copy(),cv2.COLOR_GRAY2BGR)
    #    pic7            = cv2.drawContours(pic6,[pts1],0,(0,0,255),2)
    #    cv2.imshow(str(j),pic7)
    ###

    y = 80
    x = y * 6
    pts2 = np.float32([[0, 0], [x, 0], [0, y], [x, y]])
    M = cv2.getPerspectiveTransform(pts1, pts2)
    dst = cv2.warpPerspective(expect_container_pic, M, (x, y))
    # cv2.imshow('Dst', dst)
    # cv2.waitKey()
    return dst
cv2.waitKey(0)
cv2.destroyAllWindows()