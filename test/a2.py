import cv2, os, numpy as np

######################## config parameter
date_time_height                                = 70
crop_height_ratio                               = 0.7
crop_width_ratio                                = 0.6
resize_ratio1                                   = 0.6
resize_ratio2                                   = 0.7
max_area_contour                                = 25000
min_area_contour                                = 10000
kernel1                                         = np.ones((1,60),np.uint8)
kernel2                                         = np.ones((1,60),np.uint8)
min_ratio_rect                                  = 2
max_ratio_rect                                  = 10
buffer1                                         = 20
small_area                                      = 150
buf                                             = 10
tran_y                                          = 40
tran_x                                          = tran_y*10
#########################################
cwd                                             = os.getcwd()
pic_in_path                                     = os.path.join(cwd,'in')
pic_out_path                                    = os.path.join(cwd,'out')
files                                           = os.listdir(pic_in_path)
#########################################
for i in xrange(len(files)):
#i=1
#########################################                                                      
    im1                                         = cv2.imread(os.path.join(pic_in_path,files[i]),1)
    
    height1, width1 , ch                        = im1.shape
    width2                                      = int(width1  * resize_ratio1)
    height2                                     = int(height1 * resize_ratio1)
    im2                                         = cv2.resize(im1, (width2,height2)) 
    
    im3                                         = im2[date_time_height:height2,0:width2]
    im4                                         = cv2.cvtColor(im3, cv2.COLOR_BGR2GRAY)
    im6                                         = cv2.GaussianBlur(im4, (7,7), 0)
    im7                                         = cv2.threshold(im6, 220, 255, cv2.THRESH_BINARY)[1]
    
    height3, width3                             = im7.shape
    height4                                     = int(height3 * crop_height_ratio)
    width4                                      = int(width3  * crop_width_ratio)
    im8                                         = im7[0:height4,0:width4]
    
    im9                                         = cv2.morphologyEx(im8, cv2.MORPH_CLOSE, kernel1)
    cts1                                        = cv2.findContours(im9, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[1]
    for j in xrange(len(cts1)):
        area                                    = cv2.contourArea(cts1[j])
        if area < min_area_contour or area > max_area_contour :
            continue
         
        x,y,w,h                                 = cv2.boundingRect(cts1[j])
        if x == 0 or y == 0 or x+w >= width4 or y+h >= height4 :
            continue
         
        ratio_rect                              = w / float(h)
        if ratio_rect > max_ratio_rect or ratio_rect < min_ratio_rect :
            continue
         
        x1                                      = x - buffer1
        x2                                      = x + w + buffer1
        y1                                      = y - buffer1
        y2                                      = y + h + buffer1
        if x1 < 0 or y1 < 0 or x2 > width4 or y2 > height4 :
            continue
        im10                                    = im4[y1:y2,x1:x2]
        ######################################### transform
        im11                                    = cv2.GaussianBlur(im10, (3,3), 0)
        im13                                    = cv2.threshold(im11,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]        
        height5, width5                         = im13.shape
        _, ctrs, hrc    = cv2.findContours(im13,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        a_pos           = []
        a_ctr           = []
        d_pos           = {}
        for n in xrange(len(ctrs)):
            ctr_area    = cv2.contourArea(ctrs[n])
            if ctr_area < small_area :
                continue
            if hrc[0][n][3] > -1 :
                continue
            
            rect        = cv2.minAreaRect(ctrs[n])
            box1        = cv2.boxPoints(rect)
            box2        = np.int0(box1)

            a_ctr_pos   = []
            for k in box2:
                a_pos.append(k)
                a_ctr.append(n)
                a_ctr_pos.append(k)
            d_pos[n]    = a_ctr_pos

        npa_pos         = np.asarray(a_pos, dtype=np.int64)
        npa_ctr         = np.asarray(a_ctr, dtype=np.int64)
        npa_ctr.shape   =(npa_ctr.shape[0],1)
        npa_pos_ctr     = np.append(npa_pos,npa_ctr,axis=1)
            
        npa_pos_x       = npa_pos_ctr[:,0]
        npa_pos_y       = npa_pos_ctr[:,1]
        npa_pos_x_sort  = np.argsort(npa_pos_x)
        npa_pos_y_sort  = np.argsort(npa_pos_y)
        pos_leftmost    = npa_pos_ctr[npa_pos_x_sort[0]][0:2]
        pos_rightmost   = npa_pos_ctr[npa_pos_x_sort[-1]][0:2]

        ctr_leftmost    = npa_pos_ctr[npa_pos_x_sort[0]][2]
        ctr_rightmost   = npa_pos_ctr[npa_pos_x_sort[-1]][2]

        #FIND 4 POSITION
        ctr_left            = d_pos[ctr_leftmost]
        ctr_right           = d_pos[ctr_rightmost]
       
        n_ctr_left          = np.asarray(ctr_left, dtype=np.int64)
        n_ctr_right         = np.asarray(ctr_right,dtype=np.int64)
        
        n_ctr_left_sort     = np.argsort(n_ctr_left[:,0])
        n_ctr_right_sort    = np.argsort(n_ctr_right[:,0])
        
        most_left_pos       = n_ctr_left[n_ctr_left_sort[0]]
        most_right_pos      = n_ctr_right[n_ctr_right_sort[-1]]
        
        n_ctr_left_x        = np.delete(n_ctr_left, n_ctr_left_sort[0], 0)
        n_ctr_right_x       = np.delete(n_ctr_right, n_ctr_right_sort[-1], 0) 
        
        a_distance_left     = []
        a_distance_right    = []
        
        for m in xrange(3):
            temp_left1 =  abs(most_left_pos[0] - n_ctr_left_x[m][0])**2 +abs(most_left_pos[1] - n_ctr_left_x[m][1])**2
            a_distance_left.append(temp_left1)
        temp_left2 = sorted(range(len(a_distance_left)),key=lambda k: a_distance_left[k])
        n_ctr_left_y = np.delete(n_ctr_left_x, temp_left2[2], 0)     
        
        for m in xrange(3):
            temp_right1 =  abs(most_right_pos[0] - n_ctr_right_x[m][0])**2 +abs(most_right_pos[1] - n_ctr_right_x[m][1])**2
            a_distance_right.append(temp_right1)
        temp_right2 = sorted(range(len(a_distance_right)),key=lambda k: a_distance_right[k])
        n_ctr_right_y = np.delete(n_ctr_right_x, temp_right2[2], 0)         

        left1               = most_left_pos
        left2               = n_ctr_left_y[0]
        left3               = n_ctr_left_y[1]
    
        right1              = most_right_pos
        right2              = n_ctr_right_y[0]
        right3              = n_ctr_right_y[1]
        
        dis_left2           = abs(left1[0]-left2[0])**2 + abs(left1[1]-left2[1])**2
        dis_left3           = abs(left1[0]-left3[0])**2 + abs(left1[1]-left3[1])**2
      
        dis_right2          = abs(right1[0]-right2[0])**2 + abs(right1[1]-right2[1])**2
        dis_right3          = abs(right1[0]-right3[0])**2 + abs(right1[1]-right3[1])**2
       
        if dis_left2 < dis_left3 :
            left2 = left3
        if dis_right2 < dis_right3 :
            right2 = right3

        if left1[1]<left2[1] :
            pos_topleft     = left1
            pos_bottomleft  = left2
        else :
            pos_topleft     = left2 
            pos_bottomleft  = left1
        
        if right1[1]<right2[1] :
            pos_topright    = right1
            pos_bottomright = right2
        else :
            pos_topright    = right2
            pos_bottomright = right1       
     
        #BUFFER
        buf_pos_topleft         = pos_topleft
        buf_pos_bottomleft      = pos_bottomleft
        buf_pos_topright        = pos_topright
        buf_pos_bottomright     = pos_bottomright
        
        buf_pos_topleft[0]      = buf_pos_topleft[0]-buf
        buf_pos_topleft[1]      = buf_pos_topleft[1]-buf
        buf_pos_topright[0]     = buf_pos_topright[0]+buf
        buf_pos_topright[1]     = buf_pos_topright[1]-buf
        buf_pos_bottomleft[0]   = buf_pos_bottomleft[0]-buf
        buf_pos_bottomleft[1]   = buf_pos_bottomleft[1]+buf
        buf_pos_bottomright[0]  = buf_pos_bottomright[0]+buf
        buf_pos_bottomright[1]  = buf_pos_bottomright[1]+buf
          
        pts1            = np.float32([buf_pos_topleft,buf_pos_topright,
                                      buf_pos_bottomleft,buf_pos_bottomright]) 
        pts2            = np.float32([[0,0],[tran_x,0],[0,tran_y],[tran_x,tran_y]])
        M               = cv2.getPerspectiveTransform(pts1,pts2)
        im14            = cv2.warpPerspective(im13,M,(tran_x,tran_y))
        cv2.imwrite(os.path.join(pic_out_path,files[i]),im14)
