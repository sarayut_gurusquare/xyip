import os
import cv2
import shutil

cwd                 = os.getcwd()
path_out_folder     = os.path.join(cwd,'output')
path_in_folder      = os.path.join(cwd,'input')

files=os.walk(path_in_folder).next()[2]
for i in xrange(len(files)):
#for i in xrange(60):
    print i
    file_size = os.stat(os.path.join(path_in_folder,files[i])).st_size
    if file_size==0:
        continue
    basename, extension=os.path.splitext(files[i])
    if extension == '.txt':
        f = open(os.path.join(path_in_folder,files[i]),'r')
        read = f.read()
        p1=read.find('plate=')
        if(p1==-1):
            continue
        p2=read.find('loc=',p1)
        p3=read.find('>',p2)
        p4=p1+len('province=')
        
        #lpr location
        lpr_location=read[p2+4:p3].split(',')
        
        pic_file = basename+'.jpg'
        pic1=cv2.imread(os.path.join(path_in_folder,pic_file),1)
#        pic1 = cv2.rectangle(pic1,(int(lpr[0]),int(lpr[1])),(int(lpr[2]),int(lpr[3])),(0,255,0),3)
        
        
        pos_x_center_1 = (int(lpr_location[0])+int(lpr_location[2]))/2
        pic_name = basename+'.jpg'
        pic1=cv2.imread(os.path.join(path_in_folder,pic_name),0)
        width_pic=pic1.shape[1]
        
        pos_x_center_2 = width_pic/2
        if abs(pos_x_center_1 - pos_x_center_2) < 5 :
#             print pos_x_center_1
#             print pos_x_center_2
#             print 'xxx'
            shutil.copyfile(os.path.join(path_in_folder,pic_name),os.path.join(path_out_folder,pic_name))
            continue
cv2.waitKey(0)
cv2.destroyAllWindows()