"""

"""

import cv2
import os
import numpy as np
import subprocess
import time

def f_most_pos(order_number_of_contour,all_contours):
    cnt=all_contours[order_number_of_contour]
    leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
    rightmost = tuple(cnt[cnt[:,:,0].argmax()][0])
    topmost = tuple(cnt[cnt[:,:,1].argmin()][0])
    bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])
    return leftmost, rightmost, topmost, bottommost

small_area              = 250
buf                     = 10
cwd                     = os.getcwd()
path_input_folder       = os.path.join(cwd,'input_a4')
path_output_folder      = os.path.join(cwd,'output_a4')
files                   = os.listdir(path_input_folder)
command_tes             = os.path.join(cwd, 'Tesseract-OCR','tesseract.exe')


for i in xrange(len(files)) :
    basename            =os.path.splitext(files[i])[0]
    extension           =os.path.splitext(files[i])[1]
    if extension == '.jpg' :
        path_pic        = os.path.join(path_input_folder,files[i])
        pic0            = cv2.imread(path_pic)
        pic1            = cv2.cvtColor(pic0,cv2.COLOR_BGR2GRAY)
        pic2            = cv2.GaussianBlur(pic1,(5,5),0)
        ret3,pic3       = cv2.threshold(pic2,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        ###
#        cv2.imshow('binarized',pic3)
        ###
        #add black buffer around pic
        x1, y1          = pic1.shape
        pic_black       = np.zeros((x1+(2*buf),y1+(2*buf)),dtype=np.uint8)
        pic_letters     = pic_black.copy()
        pic_letters[buf:buf+x1,buf:buf+y1]=pic3
        ###
#        cv2.imshow('buf',pic_letters)
        ###
        #dictionary key = j, value = horizontal position
        d_hp            = {}
        d_pic           = {}
        #array whose members are j sorted by horizontal position left to right
        a_hp            = []
        _, ctrs, hrc    = cv2.findContours(pic_letters,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)        
        for j in xrange(len(ctrs)) :
            ctr_area    = cv2.contourArea(ctrs[j])
            if ctr_area < small_area :
                continue
            #eleminate contours who have parent 
            if hrc[0][j][3] > -1 :
                continue
            #if contour has childeren, then get all children
            a_child     = []
            if hrc[0][j][2] > -1 :
                for k in xrange(len(ctrs)) :
                    if k == j :
                        continue
                    elif hrc[0][k][3] == j :
                        a_child.append(k)
            pic_letter  = cv2.drawContours(pic_black.copy(),[ctrs[j]],0,255,-1)
            if len(a_child) > 0 :
                for m in xrange(len(a_child)):
                    pic_child   = cv2.drawContours(pic_black.copy(),[ctrs[a_child[m]]],0,255,-1)
                    pic_letter  = pic_letter - pic_child
            x2,y2,w2,h2         = cv2.boundingRect(ctrs[j])
            pic_letter_small    = pic_letter[y2-buf:y2+h2+buf,x2-buf:x2+w2+buf]
            ###
#            cv2.imshow('letter'+str(j),img)
#            cv2.imshow(str(j),pic_letter)
            ###
            #find order of letter from left to right
            d_hp[j]             = x2
            d_pic[j]            = pic_letter_small
        for x, y in sorted(d_hp.items(),key=lambda a:a[1]):
            a_hp.append(x)
        ###
#        start                   = time.time()    
        ###
        result                  = ''
        for j in xrange(len(a_hp)) :
            
            letter_save_name1   = basename+'_'+str(j)
            letter_save_name2   = letter_save_name1 + extension
            cv2.imwrite(os.path.join(path_output_folder,letter_save_name2), d_pic[a_hp[j]])
            command             = [command_tes,\
                                   os.path.join(path_output_folder,letter_save_name2),\
                                   os.path.join(path_output_folder,letter_save_name1),\
                                   '--psm','10','--oem','0']
            subprocess.call(command)
        ###
#        stop                    =  time.time()                
#        print 'time: '+str(stop - start)
#        print 'len:  '+str(len(a_hp))
#        print 'xxxxxxxxxxxxx'
        ###
            f                   = open(os.path.join(path_output_folder,letter_save_name1+'.txt'),'r')
            read                = f.read()
            if len(read) > 0 :
                result          = result+read[0]
            else :
                result          = result+'_'         
            f.close()
        print result
       















        
    
cv2.waitKey(0)
cv2.destroyAllWindows()

