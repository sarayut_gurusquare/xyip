import os
import cv2
import glob
import datetime
import time
import numpy as np
import shutil
import re
import sys

# Relative folders
main_path = '.'
temp_save_pic_path = 'tmp'
result_pic_container_no_path = 'result_picture'
result_ocr_container_no_path = 'result_container_no'
result_ocr_container_no_file = 'container_no.txt'
minimum_length_result = 8
list_folders = [temp_save_pic_path, result_ocr_container_no_path, result_pic_container_no_path]

def update_list_folders():
    global list_folders
    list_folders = [temp_save_pic_path, result_ocr_container_no_path, result_pic_container_no_path]

crop_expand_size_pixels = 10

# Config to resize image
full_width = 1024
full_height = 768

# Capture image ratio
min_ratio = 2.5
max_ratio = 3.5


def ocr_container_no_image(file_name):
    img = cv2.imread(file_name)
    img = __container_no_detection(img)

    cv2.imshow('Find Contours', img)
    cv2.waitKey()

def __container_no_detection(img):
    now_datetime = datetime.datetime.now()
    start_detect_date_time = now_datetime.strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
    print_detect_date_time = now_datetime.strftime("%Y-%m-%d %H:%M:%S:%f")[:-3]

    print 'Start container detect : ' + start_detect_date_time
    # TODO : Clean files every time
    # clean_files_in_folder(temp_save_pic_path)

    # img = cv2.resize(img, (full_width, full_height))

    img2gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img2gray = cv2.medianBlur(img2gray, 5)
    img2gray = cv2.GaussianBlur(img2gray, (7,7), 0)
    # cv2.imshow('img2gray', img2gray)
    # cv2.waitKey()

    ret, mask = cv2.threshold(img2gray, 200, 255, cv2.THRESH_BINARY)
    # cv2.imshow('mask', mask)
    # cv2.waitKey()

    image_final = cv2.bitwise_and(img2gray, img2gray, mask=mask)
    # cv2.imshow('First Bitwise_and', image_final)
    # cv2.waitKey()

    ret, new_img = cv2.threshold(image_final, 180, 255, cv2.THRESH_BINARY)  # for black text , cv.THRESH_BINARY_INV

    # print 'Finish Binarize : ' + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
    binarized_img = new_img
    # cv2.imshow('First Binarization', new_img)
    # cv2.waitKey()

    # 09/10/2017 Crop the left panel
    # x from top->down
    # y from left->right

    x_position = 0  # height_position
    height = full_height - x_position
    y_position = 100  # width_position
    width = full_width * 3 / 4

    # NOTE: its img[y: y + h, x: x + w] and *not* img[x: x + w, y: y + h]
    new_img = new_img[y_position:height, x_position:width]


    # cv2.imshow('Crop', new_img)
    # cv2.waitKey()

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
    new_img = cv2.dilate(new_img, kernel, iterations=9)  # dilate , more the iteration more the dilation
    # cv2.imshow('Dilate', new_img)
    # cv2.waitKey()

    # print 'Crop & Dilate : ' + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]

    image, contours, hier = cv2.findContours(new_img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    str_datetime = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]

    counter = 1

    # TYPE 1 : Save images each contours and check one by one with tesseract
    for cnt in contours:
        # cv2.drawContours(img, [cnt], 0, (0, 255, 0), 2)
        [x, y, w, h] = cv2.boundingRect(cnt)
        x += x_position
        y += y_position
        # print 'Check Coutour Size %', (w * h)
        # print 'x = ' + str(x)
        # print 'y = ' + str(y)
        # print 'w = ' + str(w)
        # print 'h = ' + str(h)

        # filter contours that is adjacent to the edge of image and size (w * h) is less than 40,000 pixels^2 with w > h
        # and ratio h/w
        ratio = w / float(h)

        if(x > 0 and x + w < full_width and y > 0 and y + h < full_height and w > h and (w * h > 40000)
           and (ratio >= min_ratio) and (ratio <= max_ratio)):
            # TODO : Call container_no_recognition(img) and write result to text file
            # Write crop images
            # print 'x = ' + str(x)
            # print 'y = ' + str(y)
            # print 'w = ' + str(w)
            # print 'h = ' + str(h)

            # Save contour of the image to tmp path
            # crop_img = img[y:y+h, x:x+w]
            # Expand size
            crop_img = img[y - crop_expand_size_pixels :y + h + crop_expand_size_pixels, x - crop_expand_size_pixels :x + w + crop_expand_size_pixels]


            # # save image from Binarized image
            # crop_img = binarized_img[y:y+h, x:x+w]

            # cv2.imshow('Crop', crop_img)
            # cv2.waitKey()
            ocr_container_no_result = __container_no_recognition(crop_img)
            print 'Detected Container No = ' + ocr_container_no_result
            if not(ocr_container_no_result == '') and len(ocr_container_no_result) >= minimum_length_result  and match_container_no_format(ocr_container_no_result):
                # print 'Add contour'
                result_image_path = os.path.join(result_pic_container_no_path, str_datetime + '-' + str(counter) + '.jpg')
                print 'Save image to : ' + result_image_path
                cv2.imwrite(result_image_path, crop_img)

                result_text_path = os.path.join(result_ocr_container_no_path, result_ocr_container_no_file)
                print 'Save result to : ' + result_text_path
                # with open(os.path.join(result_ocr_container_no_path, str_datetime + '-' + str(counter) + '.txt'), "w") as text_file:
                #     text_file.write(ocr_container_no_result)
                with open(result_text_path, 'a') as text_file: # a is append mode
                    text_file.write(ocr_container_no_result + "\t" + result_image_path + "\t" + print_detect_date_time + '\n')

                counter += 1

                cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)

            # cv2.imshow('Add Contours', img)
            # cv2.waitKey()

        # else:
            # print "Skip contour"


    # # TYPE 2 : Save images all contours and check multiple in the same time
    # counter = 1
    # # clean_files_in_folder(temp_save_pic_path)
    # # create_config_folders()
    #
    # str_datetime = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
    # # crop_img_file_list = open(os.path.join(temp_save_pic_path, 'list.txt'), 'w')
    #
    #
    # # print str_datetime
    #
    # for cnt in contours:
    #     [x, y, w, h] = cv2.boundingRect(cnt)
    #     x += x_position
    #     y += y_position
    #     # print 'Check Coutour Size %', (w * h)
    #     # print 'x = ' + str(x)
    #     # print 'y = ' + str(y)
    #     # print 'w = ' + str(w)
    #     # print 'h = ' + str(h)
    #
    #     # filter contours that is adjacent to the edge of image and size (w * h) is less than 40,000 pixels^2 with w > h
    #     # and ratio h/w
    #     ratio = w / float(h)
    #
    #     if(x > 0 and x + w < full_width and y > 0 and y + h < full_height and w > h and (w * h > 40000)
    #             and (ratio >= min_ratio) and (ratio <= max_ratio)):
    #         # print w/float(h)
    #
    #         # TODO : Call container_no_recognition(img) and write result to text file
    #         # Write crop images
    #         # print 'x = ' + str(x)
    #         # print 'y = ' + str(y)
    #         # print 'w = ' + str(w)
    #         # print 'h = ' + str(h)
    #
    #         # Save contour of the image to tmp path
    #         # crop_img = img[y:y+h, x:x+w]
    #         # Expand size
    #         crop_img = img[y - crop_expand_size_pixels :y + h + crop_expand_size_pixels, x - crop_expand_size_pixels :x + w + crop_expand_size_pixels]
    #         cv2.imwrite(os.path.join(temp_save_pic_path, str_datetime + '-' + str(counter) + '.jpg'), crop_img)
    #         counter += 1
    #
    #         # # save image from Binarized image
    #         # crop_img = binarized_img[y:y+h, x:x+w]
    #
    #         # cv2.imshow('Crop', crop_img)
    #         # cv2.waitKey()
    #
    #         # print 'Add contour'
    #         cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2)
    #         # cv2.imshow('Add Contours', img)
    #         # cv2.waitKey()
    #
    #     # else:
    #         # print "Skip contour"
    #
    # # TYPE 2 : OCR and read result only one time and recheck result
    # # ocr_container_no_result = __container_no_recognition(crop_img)
    # # print 'Detected Container No = ' + ocr_container_no_result

    return img

def match_container_no_format(str_container_no):
    pattern = re.compile("^([A-Z0-9 \(\)]+)+$")

    # print bool(pattern.match(str_container_no))
    return bool(pattern.match(str_container_no))

def create_config_folders():
    update_list_folders()

    for path in list_folders:
        # if not(os.path.isdir(temp_save_pic_path)):
        if not (os.path.isdir(path)):
            print 'Create Directory : ' + path
            os.makedirs(path)
        else:
            print 'Skip : ' + path

def clean_config_folders():
    for path in list_folders:
        # if not(os.path.isdir(temp_save_pic_path)):
        files = glob.glob(path)
        for f in files:
            # os.delete(f)
            shutil.rmtree(path)

def clean_files_in_folder(file_path):
    files = glob.glob(file_path)
    for f in files:
        # os.delete(f)
        shutil.rmtree(file_path)

# Call Tessaract and return result
def __container_no_recognition(crop_image):
    print "Start Recognize : " + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
    result = -1
    # Get Contour and save image
    cv2.imwrite(os.path.join(temp_save_pic_path, 'temp.jpg'), crop_image)

    # Call Tesseract
    print 'Tesseract result will be save at : ' + os.path.join(temp_save_pic_path, 'result')
    command = os.path.join(main_path, 'Tesseract-OCR','tesseract.exe') + ' ' + os.path.join(temp_save_pic_path, 'temp.jpg') + ' ' \
              + os.path.join(temp_save_pic_path, 'result') + ' --psm 13 --oem1'

    # print command
    # print os.system(command)

    # Execute Tesseract
    os.system(command)

    # Read result
    with open(os.path.join(temp_save_pic_path, 'result.txt'), 'r') as read_file:
        result = read_file.read()

    print "End Recognize : " + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
    # Return result
    return str(result).strip()


# TODO : Call Tessaract and return result from multiple contours
def __container_no_recognition_multiple_contours():
    result = -1
    return result



def start_container_no_recognition(input_video_path, output_result_path):
    cap = cv2.VideoCapture(input_video_path)

    global main_path
    main_path = os.path.dirname(os.path.realpath(__file__))
    print 'Main Path = ' + main_path

    global result_ocr_container_no_path
    result_ocr_container_no_path = os.path.join(main_path, os.path.dirname(output_result_path))
    global result_ocr_container_no_file
    result_ocr_container_no_file =  os.path.basename(output_result_path)
    global temp_save_pic_path
    temp_save_pic_path = os.path.join(main_path, temp_save_pic_path)
    global result_pic_container_no_path
    result_pic_container_no_path = os.path.join(main_path, result_pic_container_no_path)

    # IF output_result_path is directory, then use default setting.
    # if(os.path.isdir(output_result_path)):
    #     clean_files_in_folder(output_result_path)
    #     # result_ocr_container_no_path = output_result_path
    #     # create_config_folders()
    # elif(os.path.exists(output_result_path)): # If file exist then delete file and set value
    #     clean_files_in_folder(output_result_path)
    #     # result_ocr_container_no_path = os.path.dirname(output_result_path)
    #     # result_ocr_container_no_file = os.path.basename(output_result_path)
    # else: # If no specify, use defaults
    #     # result_pic_container_no_path = 'result_picture'
    #     # result_ocr_container_no_path = 'result_container_no'
    #     # result_ocr_container_no_file = 'container_no.txt'
    #     clean_files_in_folder(output_result_path)
    #     # create_config_folders()


    print 'Container No result will be at : ' + result_ocr_container_no_path + ' / ' + result_ocr_container_no_file
    print 'Container No picture will be at : ' + result_pic_container_no_path
    print 'Temp Path will be at : ' + temp_save_pic_path

    # Clean old tmp files
    # clean_files_in_folder(temp_save_pic_path)
    # clean_files_in_folder(result_ocr_container_no_path)
    # clean_files_in_folder(result_pic_container_no_path)
    create_config_folders()


    # Start capture and recognition
    while (cap.isOpened()):
        ret, frame = cap.read()

        frame = cv2.resize(frame, (full_width, full_height))
        __container_no_detection(frame)

    cap.release()
    cv2.destroyAllWindows()




def play_video_poc(file_name):

    cap = cv2.VideoCapture(file_name)
    # cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640);
    # cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 360);

    # Clean old tmp files
    # clean_files_in_folder(temp_save_pic_path)
    # clean_files_in_folder(result_ocr_container_no_path)
    clean_config_folders()
    create_config_folders()

    skip_frame = 3
    count_frame = 0

    while (cap.isOpened()):
        ret, frame = cap.read()
        print "Frame : " + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")[:-3]
        # ret = cap.set(3, full_width)
        # ret = cap.set(4, full_height)

        frame = cv2.resize(frame, (full_width, full_height))

        # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # cv2.imshow('frame', gray)
        # if count_frame % 12 == 0:


        cv2.imshow('frame', __container_no_detection(frame))
        count_frame = 0
        # else:
        #     cv2.imshow('frame',frame)
        # count_frame +=1

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

if __name__ == '__main__':
    print "Internal main start running!"
    print "OpenCV version = " + cv2.__version__

    # print sys.argv.__len__()

    if(sys.argv.__len__() == 3):
        # Start process
        # start_container_no_recognition()
        start_container_no_recognition(sys.argv[1], sys.argv[2])
    else:
        print 'Usage : python main.py <input_video_path> <result_path>'
        print 'Example : python main.py vdo/test.avi result/result.txt'

    # # ===Test Images===
    # for file in glob.glob('./img_test/*.jpg'):
    #     if os.path.isfile(file):
    #         print 'Process file = ' + file
    #         # detect_color(file)
    #         ocr_container_no_image(file)


    # # ===Test VDO===
    # filename = 'vdo/front_25_9_2017_1_28_51PM_004.avi'
    # filename = 'vdo/Top_25_9_2017_4_54_54PM.avi'
    # filename = 'vdo/Top_25_9_2017_5_16_59PM.avi'
    # play_video_poc(filename)
    # captureImgFromVideo(filename, 'outputVdo')

