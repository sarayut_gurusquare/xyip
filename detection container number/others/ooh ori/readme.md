# Truck Container Number Detector

## Prerequisite
* Python 2.7 
	* https://www.python.org/ 
* Add environment variables ใน PATH เพื่อให้เรียก python และ pip ได้
	* C:\Python27
	* C:\Python27\Scripts
* ลง Numpy 
	* https://sourceforge.net/projects/numpy/
* ลง OpenCV 3.3.0
	* https://opencv.org
* Install OpenCV แล้ว Copy File cv.pyd 
	* จาก ~\opencv\build\python\2.7\x86 (ลอง x64 แล้วไม่ได้)
	* ไว้ที่ C:\Python27\Lib\site-packages

## Usage
python main.py <video_path> <result_file_path.txt>
