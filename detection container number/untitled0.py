# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 14:32:35 2017

@author: gus
"""

"""

"""

import cv2
import os
import numpy as np

def f_most_pos(order_number_of_contour,all_contours):
    cnt=all_contours[order_number_of_contour]
    leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
    rightmost = tuple(cnt[cnt[:,:,0].argmax()][0])
    topmost = tuple(cnt[cnt[:,:,1].argmin()][0])
    bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])
    return leftmost, rightmost, topmost, bottommost

small_area          = 150
buf                 = 10
cwd                 = os.getcwd()
path_input_folder   = os.path.join(cwd,'input_a3')
path_output_folder  = os.path.join(cwd,'output_a3')
files               = os.listdir(path_input_folder)

#for i in xrange(len(files)):
i=0
if files[i].endswith('.png') or files[i].endswith('.jpg') :

    path_pic        = os.path.join(path_input_folder,files[i])
    pic0            = cv2.imread(path_pic)
    pic1            = cv2.cvtColor(pic0,cv2.COLOR_BGR2GRAY)
    pic2            = cv2.GaussianBlur(pic1,(5,5),0)
    ret3,pic3       = cv2.threshold(pic2,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    ###
#        cv2.imshow('ori_'+files[i],pic1)
#    cv2.imshow('pic2',pic2)
#    cv2.imshow('pic3',pic3)
    ###
    _, ctrs, hrc    = cv2.findContours(pic3.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    a_pos           = []
    a_ctr           = []
    d_pos           = {}
    for j in xrange(len(ctrs)):
        ctr_area    = cv2.contourArea(ctrs[j])
        if ctr_area < small_area :
            continue
        if hrc[0][j][3] > -1 :
            continue
        
        rect        = cv2.minAreaRect(ctrs[j])
        box1        = cv2.boxPoints(rect)
        box2        = np.int0(box1)
        ###
#            pic4        = cv2.cvtColor(pic1.copy(),cv2.COLOR_GRAY2BGR)
#            pic5        = cv2.drawContours(pic4,[box2],0,(0,0,255),2)
#            cv2.imshow(str(j),pic5)    
        ###
        a_ctr_pos   = []
        for k in box2:
            a_pos.append(k)
            a_ctr.append(j)
            a_ctr_pos.append(k)
        d_pos[j]    = a_ctr_pos
            
    npa_pos         = np.asarray(a_pos, dtype=np.int64)
    npa_ctr         = np.asarray(a_ctr, dtype=np.int64)
    npa_ctr.shape   =(npa_ctr.shape[0],1)
    npa_pos_ctr     = np.append(npa_pos,npa_ctr,axis=1)
#        
    npa_pos_x       = npa_pos_ctr[:,0]
    npa_pos_y       = npa_pos_ctr[:,1]
    npa_pos_x_sort  = np.argsort(npa_pos_x)
    npa_pos_y_sort  = np.argsort(npa_pos_y)
    pos_leftmost    = npa_pos_ctr[npa_pos_x_sort[0]][0:2]
    pos_rightmost   = npa_pos_ctr[npa_pos_x_sort[-1]][0:2]
#    pos_topmost     = npa_pos_ctr[npa_pos_y_sort[0]][0:2]
#    pos_bottommost  = npa_pos_ctr[npa_pos_y_sort[-1]][0:2] 
    
    ctr_leftmost    = npa_pos_ctr[npa_pos_x_sort[0]][2]
    ctr_rightmost   = npa_pos_ctr[npa_pos_x_sort[-1]][2]
    
    img = pic0
#    img = cv2.circle(pic0,(pos_leftmost[0],pos_leftmost[1]), 2, (0,0,255), -1)
#    img = cv2.circle(pic0,(pos_rightmost[0],pos_rightmost[1]), 2, (0,255,0), -1)
#    img = cv2.circle(pic0,(pos_topmost[0],pos_topmost[1]), 2, (255,0,0), -1)
#    img = cv2.circle(pic0,(pos_bottommost[0],pos_bottommost[1]), 2, (0,255,255), -1)

  

rect        = cv2.minAreaRect(ctrs[ctr_leftmost])
box1        = cv2.boxPoints(rect)
box2        = np.int0(box1)
img         = cv2.drawContours(img,[box2],0,(0,0,255),2)

rect        = cv2.minAreaRect(ctrs[ctr_rightmost])
box1        = cv2.boxPoints(rect)
box2        = np.int0(box1)
img         = cv2.drawContours(img,[box2],0,(0,255,0),2)
cv2.imshow('0',img)
cv2.waitKey(0)
cv2.destroyAllWindows()    