import time, os, cv2, subprocess

cwd                     = os.getcwd()
path_big_folder         = os.path.join(cwd,'input_big')
path_small_folder       = os.path.join(cwd,'input_small')
path_very_small_folder  = os.path.join(cwd,'input_very_small')

big_files               = os.listdir(path_big_folder)
small_files             = os.listdir(path_small_folder)
very_small_files        = os.listdir(path_very_small_folder)

path_tesseract          = r"C:\Program Files (x86)\Tesseract-OCR\tesseract.exe"

###
start                   = time.time()
for i in xrange(len(big_files)) :
    basename            = os.path.splitext(small_files[i])[0]
    extension           = os.path.splitext(small_files[i])[1]
    if extension       != '.jpg' :
        continue
    command             = [path_tesseract,\
                           os.path.join(path_small_folder,basename+extension),\
                           os.path.join(path_small_folder,basename),\
                           '--psm','13']
    subprocess.call(command)
stop                    = time.time()
print 'time small: '+str(stop - start)
###    
###
start                   = time.time()
for i in xrange(len(big_files)) :
    basename            = os.path.splitext(big_files[i])[0]
    extension           = os.path.splitext(big_files[i])[1]
    if extension       != '.jpg' :
        continue
    command             = [path_tesseract,\
                           os.path.join(path_big_folder,basename+extension),\
                           os.path.join(path_big_folder,basename),\
                           '--psm','13']
    subprocess.call(command)
stop                    = time.time()
print 'time big: '+str(stop - start)
### 
###
start                   = time.time()
for i in xrange(len(very_small_files)) :
    basename            = os.path.splitext(very_small_files[i])[0]
    extension           = os.path.splitext(very_small_files[i])[1]
    if extension       != '.jpg' :
        continue
    command             = [path_tesseract,\
                           os.path.join(path_very_small_folder,basename+extension),\
                           os.path.join(path_very_small_folder,basename),\
                           '--psm','13']
    subprocess.call(command)
stop                    = time.time()
print 'time very small: '+str(stop - start)
### 