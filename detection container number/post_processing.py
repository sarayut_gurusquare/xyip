import re
import string
import os

current_path = os.path.dirname(os.path.abspath(__file__))
print 'Current post processing path = ' + current_path
rePerfectPattern = re.compile('([A-Z\[]{3,5})\s([0-9a-zA-Z\s]+)\s([a-zA-Z0-9]+)')
reMatchUPattern = re.compile('([A-Z\[]{3,4}U)([0-9a-zA-Z\s]+)')
reSplitTextNumber = re.compile('([A-Z]+)(.*)')
reCharOnly = re.compile('[A-Z]+')
owner_code_set = set(line.strip() for line in open(os.path.join(current_path, 'Prefix.txt')))

replace_char_to_number_list = {
       " ": "",
       "s": "5",
       "S": "5",
       "o": "0",
       "O": "0",
       "@": "0",
       "a": "0",
       "Q": "0",
       "I": "1",
       "|": "1"
}

char2num = {
    '0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8,
    '9': 9, 'A': 10, 'B': 12, 'C': 13, 'D': 14, 'E': 15, 'F': 16, 'G': 17,
    'H': 18, 'I': 19, 'J': 20, 'K': 21, 'L': 23, 'M': 24, 'N': 25, 'O': 26,
    'P': 27, 'Q': 28, 'R': 29, 'S': 30, 'T': 31, 'U': 32, 'V': 34, 'W': 35,
    'X': 36, 'Y': 37, 'Z': 38,
}

def post_processing(current_result):
    print '=== Original : ' + current_result + ' ===='

    # Remove some unused charactor
    current_result = re.sub('[^A-Za-z0-9\s]+', '', current_result)

    # Detect owner_code, serial_number, check_digit
    for_owner_code = ''
    for_serial_number = ''
    for_check_digit = ''

    match_perfect = rePerfectPattern.match(current_result)
    if match_perfect:
        print current_result + " : " + 'Match Perfect'
        for_owner_code = match_perfect.group(1)
        for_serial_number = match_perfect.group(2)
        for_check_digit = match_perfect.group(3)
    else:
        matchU = reMatchUPattern.match(current_result)
        if matchU:
            print current_result + " : " + 'Match U at char 4-5'
            for_owner_code = matchU.group(1)
            group2 = matchU.group(2)

            if len(string.replace(group2, ' ', '')) == 6:
                for_serial_number = group2
                for_check_digit = ''
            else:
                for_serial_number = group2[:-1]
                for_check_digit = group2[-1:]
        else:
            # Split Text and Number (AAAA)(0000000)
            match = reSplitTextNumber.match(current_result)
            if match:
                # print current_result + " : " + match.group(1) + " && " + match.group(2)
                for_owner_code = match.group(1)

                group2 = match.group(2)
                # print 'Group 2 : ' + group2 + ', length = ' + str(len(string.replace(group2, ' ','')))

                if len(string.replace(group2, ' ','')) == 6:
                    for_serial_number = group2
                    for_check_digit = ''
                else:
                    for_serial_number = group2[:-1]
                    for_check_digit = group2[-1:]
            else:
                print current_result + " : " + 'Not Match.'

    # Replace characters to numbers
    # === Serial Number Section ==
    for_serial_number = replace_string_All(replace_char_to_number_list, for_serial_number)
    # print 'Result for_serial_number = ' + for_serial_number

    # === Check Digit Section ===
    for_check_digit = replace_string_All(replace_char_to_number_list, for_check_digit)
    # print 'Result for_check_digit = ' + for_check_digit

    # Replace owner code character by fuzzy match text from dictionary
    # === Owner Code Section ===
    print 'Start check owner code : ' + for_owner_code
    len_owner_code = len(for_owner_code)
    if len_owner_code > 4:  # If length more than 4 then decrease to 4
        print 'Length > 4 : Decrease by finding the distance via Prefix.txt'

        if for_owner_code[3] == 'L' and for_owner_code[4] == 'I': #'LI' in for_owner_code:
            # for_owner_code = string.replace(for_owner_code, 'LI', 'U')
            for_owner_code = for_owner_code[:-2] + 'U'
        else:
            print 'Check the correct owner code via Prefix.txt'
            for_owner_code = adjust_owner_code(for_owner_code)
    elif len_owner_code < 4:  # If length less than 4 then check less edit distance in dict
        print 'Length < 4'

        if len_owner_code == 3:
            # Check 'U' is the 3rd position of string.
            print 'Owner Code = ' + for_owner_code + ' & char3rd = ' + for_owner_code[2]
            char3rd = for_owner_code[2]
            if char3rd.upper() <> 'U':
                for_owner_code += 'U'
            for_owner_code = adjust_owner_code(for_owner_code)


    # else:  # If length equals to 4 then recheck that contain in set or not or check less edit distance in dict
    #     print 'Length == 4. Then wait to checkDigitProcess'

    # Check last digit
    print 'Start check last digit : ' + for_check_digit
    # if match_perfect:
    if len(for_owner_code) == 4 and len(for_serial_number) == 6:
        gen_check_digit = generate_check_digit(for_owner_code + for_serial_number)
        print 'Compare : ' + for_check_digit + ' vs ' + gen_check_digit
        if (gen_check_digit in for_check_digit) or for_check_digit == '' or reCharOnly.match(for_check_digit):
            for_check_digit = gen_check_digit
                # final_answer = true

        # TODO: Case for_serial_number less than 6
        # else:

    afterResult = for_owner_code + ' ' + for_serial_number + ' ' + for_check_digit
    afterResult = afterResult.strip()
    print 'After post_processing = ' + afterResult
    return afterResult


def replace_string_All(replace_list, text):
    # use these three lines to do the replacement
    replace_list = dict((re.escape(k), v) for k, v in replace_list.iteritems())
    pattern = re.compile("|".join(replace_list.keys()))
    text = pattern.sub(lambda m: replace_list[re.escape(m.group(0))], text)
    return text


def generate_check_digit(first10):

    total = sum(char2num[c] * 2 ** x for x, c in enumerate(first10))
    return str((total % 11) % 10)


def adjust_owner_code(current_owner_code):
    new_owner_code = ''
    minimum_distance = 100

    for owner_check in owner_code_set:
        current_distance = levenshtein(owner_check, current_owner_code)
        if current_distance < minimum_distance:
            minimum_distance = current_distance
            new_owner_code = owner_check

    return new_owner_code

def levenshtein(s, t):
    ''' From Wikipedia article; Iterative with two matrix rows. '''
    if s == t:
        return 0
    elif len(s) == 0:
        return len(t)
    elif len(t) == 0:
        return len(s)
    v0 = [None] * (len(t) + 1)
    v1 = [None] * (len(t) + 1)
    for i in range(len(v0)):
        v0[i] = i
    for i in range(len(s)):
        v1[0] = i + 1
        for j in range(len(t)):
            cost = 0 if s[i] == t[j] else 1
            v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
        for j in range(len(v0)):
            v0[j] = v1[j]

    return v1[len(t)]

if __name__ == '__main__':
    print 'Start post processing'
    listResult = [
        'BEAU 262894 {51',
        'BEAU 262894 5',
        'CMAU 80 1325 [1]',
        'DV8AES NYANI',
        'GLDOU 975649 5',
        'GLDU 975649 E]',
        'HDMLI 630317 Q/',
        'INYKU 337842 O',
        'INYKU 337842 O]',
        'INYKU 364084 4',
        'INYKU 364084 4]',
        'INYKU 364084 4A',
        'INYKU 364084 A]',
        'INYKU353658 9]',
        'INYKU3S1602 6]',
        'INYKU3S51602 6]',
        'INYKU3S53658 9]',
        'INYKU3SS1602 6]',
        'INYKUSS1602 6]',
        'INYKUSS51602 6]',
        'IVYKU 337842 0',
        'L HL20E9 NWOH',
        'NYKU 337842 O]',
        'NYKU 3675 1 2 a',
        'NYKU 367512 0',
        'NYKU 367512 O',
        'NYKU351602 6]',
        'NYKU3S51602 6]',
        'TCLLU47382977715}',
        'TCLU 395938 4',
        'TCLU 438297 5',
        'TCLU 438297 S',
        'TCLU 438297',
        'TCLU 4382s9 7',
        'TCLUY 438297 5',
        'TEMU 431855 Ki',
        'TEMU 431865 4',
        'TEMU 431865 T',
        'WYKU 337842 0',
        'WYKU 337842 O',
        '(AY 2 MISTER]',
        '[413 2D E',
        '[9 2O91SSNMAN',
        '[9 Z2O9LSENMAN',
        '[9 ZO91SENMAN',
        '[9 ZO91SENMANI',
        '[9 ZO9LSENMANI',
        '[FAE SVU]',
        '[NYKU 364084 4]',
        '[P NMANI',
        '0 ZLSL9E NMAN',
        '0 ZLSL9E NXMAN',
        '364084 4]',
        '6 8S59ESSNMANN'

    ]
    for result in listResult:
        afterResult = post_processing(result)
        # print 'Before = ' + result + ', After = ' + afterResult