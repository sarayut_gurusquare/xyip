"""

"""

import cv2
import os
import numpy as np



small_area          = 150
buf                 = 10
cwd                 = os.getcwd()
path_input_folder   = os.path.join(cwd,'input_a3')
path_output_folder  = os.path.join(cwd,'output_a3')
files               = os.listdir(path_input_folder)

for i in xrange(len(files)):

    if files[i].endswith('.png') or files[i].endswith('.jpg') :
    
        path_pic        = os.path.join(path_input_folder,files[i])
        pic0            = cv2.imread(path_pic)
        pic1            = cv2.cvtColor(pic0,cv2.COLOR_BGR2GRAY)
        pic2            = cv2.GaussianBlur(pic1,(5,5),0)
        ret3,pic3       = cv2.threshold(pic2,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        ###
    #        cv2.imshow('ori_'+files[i],pic1)
    #    cv2.imshow('pic2',pic2)
    #    cv2.imshow('pic3',pic3)
        ###
        _, ctrs, hrc    = cv2.findContours(pic3.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        a_pos           = []
        a_ctr           = []
        d_pos           = {}
        for j in xrange(len(ctrs)):
            ctr_area    = cv2.contourArea(ctrs[j])
            if ctr_area < small_area :
                continue
            if hrc[0][j][3] > -1 :
                continue
            
            rect        = cv2.minAreaRect(ctrs[j])
            box1        = cv2.boxPoints(rect)
            box2        = np.int0(box1)
            ###
    #            pic4        = cv2.cvtColor(pic1.copy(),cv2.COLOR_GRAY2BGR)
    #            pic5        = cv2.drawContours(pic4,[box2],0,(0,0,255),2)
    #            cv2.imshow(str(j),pic5)    
            ###
            a_ctr_pos   = []
            for k in box2:
                a_pos.append(k)
                a_ctr.append(j)
                a_ctr_pos.append(k)
            d_pos[j]    = a_ctr_pos
                
        npa_pos         = np.asarray(a_pos, dtype=np.int64)
        npa_ctr         = np.asarray(a_ctr, dtype=np.int64)
        npa_ctr.shape   =(npa_ctr.shape[0],1)
        npa_pos_ctr     = np.append(npa_pos,npa_ctr,axis=1)
    #        
        npa_pos_x       = npa_pos_ctr[:,0]
        npa_pos_y       = npa_pos_ctr[:,1]
        npa_pos_x_sort  = np.argsort(npa_pos_x)
        npa_pos_y_sort  = np.argsort(npa_pos_y)
        pos_leftmost    = npa_pos_ctr[npa_pos_x_sort[0]][0:2]
        pos_rightmost   = npa_pos_ctr[npa_pos_x_sort[-1]][0:2]
    #    pos_topmost     = npa_pos_ctr[npa_pos_y_sort[0]][0:2]
    #    pos_bottommost  = npa_pos_ctr[npa_pos_y_sort[-1]][0:2] 
        
        ctr_leftmost    = npa_pos_ctr[npa_pos_x_sort[0]][2]
        ctr_rightmost   = npa_pos_ctr[npa_pos_x_sort[-1]][2]
        
        img = pic0
    #    img = cv2.circle(pic0,(pos_leftmost[0],pos_leftmost[1]), 2, (0,0,255), -1)
    #    img = cv2.circle(pic0,(pos_rightmost[0],pos_rightmost[1]), 2, (0,255,0), -1)
    #    img = cv2.circle(pic0,(pos_topmost[0],pos_topmost[1]), 2, (255,0,0), -1)
    #    img = cv2.circle(pic0,(pos_bottommost[0],pos_bottommost[1]), 2, (0,255,255), -1)
    
      
    
    #rect        = cv2.minAreaRect(ctrs[ctr_leftmost])
    #box1        = cv2.boxPoints(rect)
    #box2        = np.int0(box1)
    #img         = cv2.drawContours(img,[box2],0,(0,0,255),2)
    #
    #rect        = cv2.minAreaRect(ctrs[ctr_rightmost])
    #box1        = cv2.boxPoints(rect)
    #box2        = np.int0(box1)
    #img         = cv2.drawContours(img,[box2],0,(0,255,0),2)
    #cv2.imshow('0',img)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()    
    
        
    #    FIND 4 POSITION
        ctr_left            = d_pos[ctr_leftmost]
        ctr_right           = d_pos[ctr_rightmost]
       
        n_ctr_left          = np.asarray(ctr_left, dtype=np.int64)
        n_ctr_right         = np.asarray(ctr_right,dtype=np.int64)
        
        n_ctr_left_sort     = np.argsort(n_ctr_left[:,0])
        n_ctr_right_sort    = np.argsort(n_ctr_right[:,0])
        
        most_left_pos       = n_ctr_left[n_ctr_left_sort[0]]
        most_right_pos      = n_ctr_right[n_ctr_right_sort[-1]]
        
        n_ctr_left_x        = np.delete(n_ctr_left, n_ctr_left_sort[0], 0)
        n_ctr_right_x       = np.delete(n_ctr_right, n_ctr_right_sort[-1], 0) 
        
        a_distance_left     = []
        a_distance_right    = []
        
        for m in xrange(3):
            temp_left1 =  abs(most_left_pos[0] - n_ctr_left_x[m][0])**2 +abs(most_left_pos[1] - n_ctr_left_x[m][1])**2
            a_distance_left.append(temp_left1)
        temp_left2 = sorted(range(len(a_distance_left)),key=lambda k: a_distance_left[k])
        n_ctr_left_y = np.delete(n_ctr_left_x, temp_left2[2], 0)     
        
        for m in xrange(3):
            temp_right1 =  abs(most_right_pos[0] - n_ctr_right_x[m][0])**2 +abs(most_right_pos[1] - n_ctr_right_x[m][1])**2
            a_distance_right.append(temp_right1)
        temp_right2 = sorted(range(len(a_distance_right)),key=lambda k: a_distance_right[k])
        n_ctr_right_y = np.delete(n_ctr_right_x, temp_right2[2], 0)         
        
        
        
        
    
    ###########################################
        
        left1               = most_left_pos
        left2               = n_ctr_left_y[0]
        left3               = n_ctr_left_y[1]
    
        right1              = most_right_pos
        right2              = n_ctr_right_y[0]
        right3              = n_ctr_right_y[1]
        
        img = pic0
    #    img = cv2.circle(pic0,(left1[0],left1[1]), 2, (0,0,255), -1)
    #    img = cv2.circle(pic0,(left2[0],left2[1]), 2, (0,255,0), -1)
    #    img = cv2.circle(pic0,(left3[0],left3[1]), 2, (255,0,0), -1)
        
    #    img = cv2.circle(pic0,(right1[0],right1[1]), 2, (0,0,255), -1)
    #    img = cv2.circle(pic0,(right2[0],right2[1]), 2, (0,255,0), -1)
    #    img = cv2.circle(pic0,(right3[0],right3[1]), 2, (255,0,0), -1)    
        
        
    
        dis_left2           = abs(left1[0]-left2[0])**2 + abs(left1[1]-left2[1])**2
        dis_left3           = abs(left1[0]-left3[0])**2 + abs(left1[1]-left3[1])**2
      
        dis_right2          = abs(right1[0]-right2[0])**2 + abs(right1[1]-right2[1])**2
        dis_right3          = abs(right1[0]-right3[0])**2 + abs(right1[1]-right3[1])**2
       
        if dis_left2 < dis_left3 :
            left2 = left3
        if dis_right2 < dis_right3 :
            right2 = right3
        
        
        ##########################################       
        
        
        if left1[1]<left2[1] :
            pos_topleft     = left1
            pos_bottomleft  = left2
        else :
            pos_topleft     = left2 
            pos_bottomleft  = left1
        
        if right1[1]<right2[1] :
            pos_topright    = right1
            pos_bottomright = right2
        else :
            pos_topright    = right2
            pos_bottomright = right1       
    
    #    img = pic0
    #    img = cv2.circle(pic0,(left1[0],left1[1]), 2, (0,0,255), -1)
    #    img = cv2.circle(pic0,(left2[0],left2[1]), 2, (0,255,0), -1)
    #    img = cv2.circle(pic0,(left3[0],left3[1]), 2, (255,0,0), -1)
        
    #    img = cv2.circle(pic0,(right1[0],right1[1]), 2, (0,0,255), -1)
    #    img = cv2.circle(pic0,(right2[0],right2[1]), 2, (0,255,0), -1)
    #    img = cv2.circle(pic0,(right3[0],right3[1]), 2, (255,0,0), -1)    
        
        
        
        
    #    img = cv2.circle(pic0,(pos_bottomright[0],pos_bottomright[1]), 2, (0,255,255), -1)
    
    
    
       
    #        
    #    img = pic0
    #    img = cv2.circle(pic0,(pos_topleft[0],pos_topleft[1]), 2, (0,0,255), -1)
    #    img = cv2.circle(pic0,(pos_bottomleft[0],pos_bottomleft[1]), 2, (0,255,0), -1)
    #    img = cv2.circle(pic0,(pos_topright[0],pos_topright[1]), 2, (255,0,0), -1)
    #    img = cv2.circle(pic0,(pos_bottomright[0],pos_bottomright[1]), 2, (0,255,255), -1)
    #    cv2.imshow('0',img)
    #    cv2.waitKey(0)
    #    cv2.destroyAllWindows()              
    
        #BUFFER
        buf_pos_topleft         = pos_topleft
        buf_pos_bottomleft      = pos_bottomleft
        buf_pos_topright        = pos_topright
        buf_pos_bottomright     = pos_bottomright
        
        buf_pos_topleft[0]      = buf_pos_topleft[0]-buf
        buf_pos_topleft[1]      = buf_pos_topleft[1]-buf
        buf_pos_topright[0]     = buf_pos_topright[0]+buf
        buf_pos_topright[1]     = buf_pos_topright[1]-buf
        buf_pos_bottomleft[0]   = buf_pos_bottomleft[0]-buf
        buf_pos_bottomleft[1]   = buf_pos_bottomleft[1]+buf
        buf_pos_bottomright[0]  = buf_pos_bottomright[0]+buf
        buf_pos_bottomright[1]  = buf_pos_bottomright[1]+buf
          
    #    pos_topmost[0]      = pos_topmost[0]-buf
    #    pos_topmost[1]      = pos_topmost[1]-buf
    #    pos_rightmost[0]    = pos_rightmost[0]+buf
    #    pos_rightmost[1]    = pos_rightmost[1]-buf
    #    pos_leftmost[0]     = pos_leftmost[0]-buf
    #    pos_leftmost[1]     = pos_leftmost[1]+buf
    #    pos_bottommost[0]   = pos_bottommost[0]+buf
    #    pos_bottommost[1]   = pos_bottommost[1]+buf
        
        pts1            = np.float32([buf_pos_topleft,buf_pos_topright,
                                      buf_pos_bottomleft,buf_pos_bottomright]) 
        ###
    #    img = cv2.circle(pic0,(buf_pos_topleft[0],buf_pos_topleft[1]), 2, (0,0,255), -1)
    #    img = cv2.circle(pic0,(buf_pos_topright[0],buf_pos_topright[1]), 2, (0,255,0), -1)
    #    img = cv2.circle(pic0,(buf_pos_bottomleft[0],buf_pos_bottomleft[1]), 2, (255,0,0), -1)
    #    img = cv2.circle(pic0,(buf_pos_bottomright[0],buf_pos_bottomright[1]), 2, (0,255,255), -1)
    #    cv2.imshow('0',img)
    #    cv2.waitKey(0)
    #    cv2.destroyAllWindows()  
        ###
    
        y=80
        x=y*10
        pts2            = np.float32([[0,0],[x,0],[0,y],[x,y]])
        M               = cv2.getPerspectiveTransform(pts1,pts2)
        dst             = cv2.warpPerspective(pic0,M,(x,y))
        ###
#        cv2.imshow('dst_'+files[i],dst)
        cv2.imwrite(os.path.join(path_output_folder,files[i]),dst)
            ###
        
    #    cv2.imshow('0',img)
#        cv2.waitKey(0)
#        cv2.destroyAllWindows() 









        
    


