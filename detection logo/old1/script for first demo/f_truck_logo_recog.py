import cv2
import pickle
#from skimage import exposure
from skimage import feature
#from sklearn.neighbors import KNeighborsClassifier
#import os
#import imutils
import numpy as np
import sys

#image path
#arg1=r"C:\Users\gus\Desktop\new\a3_isuzu.jpg"
#arg1=r"C:\Users\gus\Desktop\new\a15_h1_62_.jpg"

#license plate position
#arg2="778,639,964,689"
#arg2="618,365,739,397"

#text prediction path
#arg3=r"C:\Users\gus\Desktop\new\a3_isuzu_result.txt"



def f_truck_logo_recog(arg1,arg2,arg3):
    #restore learned model
    f = open(r"C:\Users\gus\Desktop\new\store.pckl", "rb")
    d_hog = pickle.load(f)
    f.close()
    
    plate_location=arg2.split(',')
    plate_width =int(plate_location[2])-int(plate_location[0])
    plate_height=int(plate_location[3])-int(plate_location[1])
    
    pic1=cv2.imread(arg1,1)
    pic1=cv2.cvtColor(pic1, cv2.COLOR_BGR2GRAY)
    #pic1 = cv2.rectangle(pic1,(int(plate_location[0]),int(plate_location[1])),(int(plate_location[2]),int(plate_location[3])),(0,255,0),3)
    horizental_center=int(plate_location[0])+int(plate_width/2)
    
    x1=horizental_center-int(1.25*plate_height)
    x2=horizental_center+int(plate_height/2)
    crop_width=int(x2-x1)
    y1=int(plate_location[3])-int(9.75*plate_height)
    y2=y1+crop_width
    #pic1 = cv2.rectangle(pic1,(x1,y1),(x2,y2),(0,255,0),1)
    crop1 = pic1[y1:y2,x1:x2]
    #cv2.imshow('c1',crop1)
    #cv2.imshow('p1',pic1)
    
    x1=horizental_center-int(1.75*plate_height)
    x2=horizental_center+int(1.75*plate_height)
    crop_width=int(x2-x1)
    y1=int(plate_location[3])-int(6*plate_height)
    y2=y1+int(0.75*crop_width)
    #pic1 = cv2.rectangle(pic1,(x1,y1),(x2,y2),(0,255,0),1)
    crop2 = pic1[y1:y2,x1:x2]
    #cv2.imshow('c2',crop2)
    #cv2.imshow('p1',pic1)
    
    logo1 = cv2.resize(crop1, (200, 100))
    logo2 = cv2.resize(crop2, (200, 100))
    
    #testing
    (H1,hogImage1) = feature.hog(logo1, orientations=9, pixels_per_cell=(10, 10),
    		cells_per_block=(2, 2), transform_sqrt=True, visualise=True)
    (H2,hogImage2) = feature.hog(logo2, orientations=9, pixels_per_cell=(10, 10),
    		cells_per_block=(2, 2), transform_sqrt=True, visualise=True)
    
    #euclidean distance
    d_i=np.array(d_hog['isuzu'])
    d_h=np.array(d_hog['hino'])
    d1_isuzu = np.linalg.norm(d_i-H1)
    d1_hino = np.linalg.norm(d_h-H1)
    d2_isuzu = np.linalg.norm(d_i-H2)
    d2_hino = np.linalg.norm(d_h-H2)
    
    a_distance_name  = ['isuzu','hino','isuzu','hino']
    a_distance_value = [d1_isuzu,d1_hino,d2_isuzu,d2_hino]
    ans1=a_distance_value.index(min(a_distance_value))
    ans2=a_distance_name[ans1]
    
    #write answer to text file
    f = open(arg3,'w')
    f.write(ans2)
    f.close()
    return ans2

arg1=sys.argv[1]
arg2=sys.argv[2]
arg3=sys.argv[3]

ans2=f_truck_logo_recog(arg1,arg2,arg3)
print ans2
#cv2.waitKey(0)
#cv2.destroyAllWindows()
