from skimage import exposure
from skimage import feature
from sklearn.neighbors import KNeighborsClassifier
import cv2
import os
import imutils

# initialize the data matrix and labels
data = []
labels = []
cwd=os.getcwd()
logo_pic_training_main_path=os.path.join(cwd,'logo_pic_training')
for xx in os.listdir(logo_pic_training_main_path):
    logo_pic_training_sub_path=os.path.join(logo_pic_training_main_path,xx)
    # load the image, convert it to grayscale, and detect edges
    make = xx
    for yy in os.listdir(logo_pic_training_sub_path):
        imagePath=os.path.join(logo_pic_training_sub_path,yy)
        image = cv2.imread(imagePath) 
#        cv2.imshow(yy,image)
#        
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#        edged = imutils.auto_canny(gray)
#        cv2.imshow(yy,edged)
        
        # find contours in the edge map, keeping only the largest one which
        # is presumed to be the car logo
#        _, cnts,_ = cv2.findContours(edged.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
#        c = max(cnts, key=cv2.contourArea)
        
        # extract the logo of the car and resize it to a canonical width
        # and height
#        (x, y, w, h) = cv2.boundingRect(c)
#        logo = gray[y:y + h, x:x + w]
        logo = cv2.resize(gray, (200, 100))   
##        cv2.imshow(yy,logo)
      
#        # extract Histogram of Oriented Gradients from the logo
        (H, hogImage) = feature.hog(logo, orientations=9, pixels_per_cell=(10, 10),
             cells_per_block=(2, 2), transform_sqrt=True, visualise=True)
 
        hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
        hogImage = hogImage.astype("uint8")
        cv2.imshow(yy, hogImage)
   
        # update the data and labels
        data.append(H) 
        labels.append(make)
cv2.waitKey(0)
cv2.destroyAllWindows()

# "train" the nearest neighbors classifier
print "[INFO] training classifier..."
model = KNeighborsClassifier(n_neighbors=1)
model.fit(data, labels)
print "[INFO] evaluating..."


logo_pic_testing_main_path=os.path.join(cwd,'logo_pic_testing')
for xx in os.listdir(logo_pic_testing_main_path):
    # load the test image, convert it to grayscale, and resize it to
    # the canonical size

    image = cv2.imread(os.path.join(logo_pic_testing_main_path,xx))
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    logo = cv2.resize(gray, (200, 100))
    
    # extract Histogram of Oriented Gradients from the test image and
    # predict the make of the car
    (H,hogImage) = feature.hog(logo, orientations=9, pixels_per_cell=(10, 10),
		cells_per_block=(2, 2), transform_sqrt=True, visualise=True)
    pred = model.predict(H.reshape(1, -1))[0]
    
	 # visualize the HOG image
    hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
    hogImage = hogImage.astype("uint8")
    cv2.imshow(xx, hogImage)
     
     
#    cv2.putText(image, pred.title(), (10, 35), cv2.FONT_HERSHEY_SIMPLEX, 1.0,
#		(0, 255, 0), 3)
#    cv2.imwrite(xx+'_out.jpg',image)
#    cv2.imshow(xx, image) 
    
cv2.waitKey(0)
cv2.destroyAllWindows()