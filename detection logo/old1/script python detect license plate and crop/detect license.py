import os
import cv2

cwd=os.getcwd()
path_text = os.path.join(cwd,'input_text')
path_pic  = os.path.join(cwd,'input_pic')

texts = os.listdir(path_text)
len_plate=len('plate=')

for i in range(len(texts)):
    t1= texts[i]
    file_size = os.stat(os.path.join(path_text,t1)).st_size
    if(file_size==0):
        continue
    f = open(os.path.join(path_text,t1),'r')
    read = f.read()
    p1=read.find('plate=')
    if(p1==-1):
        continue
    p2=read.find('loc=',p1)
    p3=read.find('>',p2)
    p4=p1+len_plate
    plate_location=read[p2+4:p3].split(',')
    plate_height  =int(plate_location[3])-int(plate_location[1])
    plate_width   =int(plate_location[2])-int(plate_location[0])
    file_name = t1[0:-4]

    pic1=os.path.join(path_pic,file_name+'.jpg')
    pic2=cv2.imread(pic1,1)
    pic3 = cv2.rectangle(pic2,(int(plate_location[0]),int(plate_location[1])),(int(plate_location[2]),int(plate_location[3])),(0,255,0),3)
    cv2.imshow(str(i),pic3)
cv2.waitKey(0)
cv2.destroyAllWindows()
    
