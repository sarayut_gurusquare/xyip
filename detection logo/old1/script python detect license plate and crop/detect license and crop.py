import os
import cv2

cwd=os.getcwd()
path_text = os.path.join(cwd,'input_text')
path_pic  = os.path.join(cwd,'input_pic')

texts = os.listdir(path_text)
len_plate=len('plate=')

for i in range(len(texts)):
    t1= texts[i]
    file_size = os.stat(os.path.join(path_text,t1)).st_size
    if(file_size==0):
        continue
    f = open(os.path.join(path_text,t1),'r')
    read = f.read()
    p1=read.find('plate=')
    if(p1==-1):
        continue
    p2=read.find('loc=',p1)
    p3=read.find('>',p2)
    p4=p1+len_plate
    plate_location=read[p2+4:p3].split(',')
    plate_width =int(plate_location[2])-int(plate_location[0])
    plate_height=int(plate_location[3])-int(plate_location[1])

    file_name = t1[0:-4]
    temp = file_name.split('_')
    ans = temp[len(temp)-1]
    pic1=os.path.join(path_pic,file_name+'.jpg')
    
    pic2=cv2.imread(pic1,1)
    #draw license plate
    pic3 = cv2.rectangle(pic2,(int(plate_location[0]),int(plate_location[1])),(int(plate_location[2]),int(plate_location[3])),(0,255,0),3)
    #draw horizontal line
#    line_width=int(plate_width/3)
    x1=int(plate_location[0])
    x2=int(plate_location[2])
    y1=int(plate_location[3])-(10*plate_height)
    y2=y1+(8*plate_height)
    pic4 = cv2.rectangle(pic3,(x1,y1),(x2,y2),(0,255,0),1)
    pic5 = pic4[y1:y2,x1:x2]
#    cv2.putText(pic4,str(plate_height),(50,50), cv2.FONT_HERSHEY_SIMPLEX, 2,(0,255,255),2,cv2.LINE_AA)
#    pic4 = cv2.line(pic3,(x1,y1),(x2,y2),(255,0,0),2)
    cv2.imshow(str(i),pic2)
cv2.waitKey(0)
cv2.destroyAllWindows()
    
