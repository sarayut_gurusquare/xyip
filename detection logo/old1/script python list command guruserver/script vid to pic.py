import cv2
import os

vid_folder_path  =r"D:\project YIP\pic and video sample truck and container\vid_lot1_and_lot2"
pic_folder_path  =r"D:\project YIP\pic and video sample truck and container\pic_lot1_and_lot2"
vids = os.listdir(vid_folder_path)

for vid in vids:
    filename = vid
    filename_without_format= filename[:-4]
    vid_path = os.path.join(vid_folder_path,filename)
    
    cap = cv2.VideoCapture(vid_path)
    ret = True
    count=0
    subpic_path=os.path.join(pic_folder_path,filename_without_format)
    os.mkdir(subpic_path) 
    
    while(cap.isOpened() and ret):
        ret, frame = cap.read()
        if(ret):
            count=count+1
            a=filename_without_format+'_'+str(count)+'.jpg'
            temp_path=os.path.join(subpic_path,a)
            cv2.imwrite(temp_path,frame)
        ##quit display by press 'q'
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    
    cap.release()
    cv2.destroyAllWindows()
    print filename
