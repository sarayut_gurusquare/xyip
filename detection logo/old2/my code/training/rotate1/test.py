import cv2
import os


cwd=os.getcwd()
in_folder_path=os.path.join(cwd,'in')
out_folder_path=os.path.join(cwd,'out')

a_angle1=[0.0,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0]
a_angle2=[]
for k in a_angle1:
    b=str(k).split('.')
    a_angle2.append('a_'+b[0]+'-'+b[1])
    

t=os.listdir(in_folder_path)
for i in t:
    if i.endswith(".png"):
        image = cv2.imread(os.path.join(in_folder_path,i),1)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        rows,cols = gray.shape
        for j in xrange(len(a_angle1)):
            M = cv2.getRotationMatrix2D((cols/2,rows/2),a_angle1[j],1)
            dst = cv2.warpAffine(gray,M,(cols,rows))
            temp1=i[0:-4]+'_'+a_angle2[j]+'.png'
            cv2.imwrite(os.path.join(out_folder_path,temp1),dst)
cv2.waitKey(0)
cv2.destroyAllWindows()