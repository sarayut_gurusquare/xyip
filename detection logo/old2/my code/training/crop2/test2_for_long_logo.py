# -*- coding: utf-8 -*-
"""
Created on Wed Oct 18 16:42:38 2017

@author: gus
"""

import cv2
import os
import numpy as np

def f_4_most_direction(each_contour):
    leftmost   = tuple(each_contour[each_contour[:,:,0].argmin()][0])
    rightmost  = tuple(each_contour[each_contour[:,:,0].argmax()][0])
    topmost    = tuple(each_contour[each_contour[:,:,1].argmin()][0])
    bottommost = tuple(each_contour[each_contour[:,:,1].argmax()][0])
    return [leftmost,rightmost,topmost,bottommost]

cwd=os.getcwd()
in_folder_path = os.path.join(cwd,'in_long')
out_folder_path = os.path.join(cwd,'out_long')

a_pic = os.listdir(in_folder_path)
trim=50

for i in xrange(len(a_pic)):
    gray=cv2.imread(os.path.join(in_folder_path,a_pic[i]),0)
    gray= cv2.bitwise_not(gray)
    h,w=gray.shape
    
    
#    gray = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
#            cv2.THRESH_BINARY,11,2)
    gray=gray[trim:h-trim,trim:w-trim]
#    color_trim=cv2.cvtColor(gray_trim,cv2.COLOR_GRAY2BGR)
    
    a_4_most=[]
    _, cnts,_ = cv2.findContours(gray.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    for j in cnts:    
        temp=f_4_most_direction(j)
        for k in temp:
            a_4_most.append(k)


   
    n_4_most=np.asarray(a_4_most)
    n_x=n_4_most[:,0]
    n_y=n_4_most[:,1]
    gray_trim_crop=gray[min(n_y):max(n_y),min(n_x):max(n_x)]
    gray_trim_crop= cv2.bitwise_not(gray_trim_crop)
    gray_trim_crop = cv2.resize(gray_trim_crop, (200, 100))
    temp=os.path.join(out_folder_path,a_pic[i])
    cv2.imwrite(temp,gray_trim_crop)