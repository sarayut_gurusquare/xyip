# -*- coding: utf-8 -*-
"""
Created on Wed Oct 18 16:42:38 2017

@author: gus
"""

import cv2
import os
import imutils
import numpy as np

def f_4_most_direction(each_contour):
    leftmost   = tuple(each_contour[each_contour[:,:,0].argmin()][0])
    rightmost  = tuple(each_contour[each_contour[:,:,0].argmax()][0])
    topmost    = tuple(each_contour[each_contour[:,:,1].argmin()][0])
    bottommost = tuple(each_contour[each_contour[:,:,1].argmax()][0])
    return [leftmost,rightmost,topmost,bottommost]



trim=50
gray= cv2.imread('a1.png',0)
gray= cv2.bitwise_not(gray)
#cv2.imshow('gray',gray)

h,w=gray.shape
gray_trim=gray[trim:h-trim,trim:w-trim]
color_trim=cv2.cvtColor(gray_trim,cv2.COLOR_GRAY2BGR)

a_4_most=[]
_, cnts,_ = cv2.findContours(gray_trim.copy(),cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

for i in cnts:    
    temp=f_4_most_direction(i)
    for j in temp:
        a_4_most.append(j)


   
n_4_most=np.asarray(a_4_most)
n_x=n_4_most[:,0]
n_y=n_4_most[:,1]
gray_trim_crop=gray_trim[min(n_y):max(n_y),min(n_x):max(n_x)]
cv2.imshow('crop',gray_trim_crop)






cv2.waitKey(0)
cv2.destroyAllWindows()

