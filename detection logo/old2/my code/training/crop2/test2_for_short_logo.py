from skimage import exposure
from skimage import feature
from sklearn.neighbors import KNeighborsClassifier
import cv2
import os
import imutils


trim=50
cwd=os.getcwd()
in_folder_path=os.path.join(cwd,'in_short')
out_folder_path=os.path.join(cwd,'out_short')

a_pic = os.listdir(in_folder_path)
cc=0
for yy in os.listdir(in_folder_path):

        imagePath=os.path.join(in_folder_path,yy)
        img = cv2.imread(imagePath)
        img_gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#        cv2.imshow(yy,image)
#        
        h,w =img_gray.shape
        img_gray=img_gray[trim:h-trim,trim:w-trim]
        edged = imutils.auto_canny(img_gray)
#        cv2.imshow(str(c),img)
        
#        
#        # find contours in the edge map, keeping only the largest one which
#        # is presumed to be the car logo
        _, cnts,_ = cv2.findContours(edged.copy(),cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        c = max(cnts, key=cv2.contourArea)
#        
#        # extract the logo of the car and resize it to a canonical width
#        # and height
        (x, y, w, h) = cv2.boundingRect(c)
        logo = img_gray[y:y + h, x:x + w]
        logo = cv2.resize(logo, (200, 100))   
        cv2.imwrite(os.path.join(out_folder_path,yy),logo)
#      
##        # extract Histogram of Oriented Gradients from the logo
#        (H, hogImage) = feature.hog(logo, orientations=9, pixels_per_cell=(10, 10),
#             cells_per_block=(2, 2), transform_sqrt=True, visualise=True)
 
#        hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
#        hogImage = hogImage.astype("uint8")
#        cv2.imshow(yy, hogImage)
        cc=cc+1

#cv2.waitKey(0)
#cv2.destroyAllWindows()




    
cv2.waitKey(0)
cv2.destroyAllWindows()