import cv2
import os
import imutils
import numpy as np
from skimage import exposure
from skimage import feature
from sklearn.neighbors import KNeighborsClassifier
import pickle

data = []
labels = []
angle = []
cwd=os.getcwd()
in_folder_path = os.path.join(cwd,'in')
out_folder_path = os.path.join(cwd,'out')

a_pic = os.listdir(in_folder_path)


for i in xrange(len(a_pic)):
    img=cv2.imread(os.path.join(in_folder_path,a_pic[i]),0)
    (H, hogImage) = feature.hog(img, orientations=9, pixels_per_cell=(10,10),
             cells_per_block=(2, 2), transform_sqrt=True, visualise=True)
    hogImage = exposure.rescale_intensity(hogImage, out_range=(0, 255))
    hogImage = hogImage.astype("uint8")
    cv2.imwrite(os.path.join(out_folder_path,a_pic[i]), hogImage)
    
    data.append(H) 
    temp1=a_pic[i].split('_')
    temp2=temp1[4].split('.')
    labels.append(temp1[0])
    angle.append(temp2[0])

d_hog={}
d_hog['labels']=labels
d_hog['angle']=angle
d_hog['data']=data
d_hog['filename']=a_pic

f = open('store.pckl', 'wb')
pickle.dump(d_hog, f)
f.close()
    
    