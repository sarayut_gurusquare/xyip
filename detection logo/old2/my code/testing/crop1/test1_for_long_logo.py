import cv2
import numpy as np
import my_f

im1 = cv2.imread('i1.png',0)
#cv2.imshow('ori',im1)

im2 = cv2.bitwise_not(im1)
im3 = cv2.threshold(im2,0,255,cv2.THRESH_OTSU)[1]
h,w = im2.shape


a_4_most=[]
cnts= cv2.findContours(im3.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)[1]
for j in cnts:    
    temp=my_f.f_4_most_direction(j)
    for k in temp:
        a_4_most.append(k)

n_4_most=np.asarray(a_4_most)
n_x=n_4_most[:,0]
n_y=n_4_most[:,1]
im4=im2[min(n_y):max(n_y),min(n_x):max(n_x)]
im4= cv2.bitwise_not(im4)
im4 = cv2.resize(im4, (200, 100))
cv2.imshow('resize',im4)
#temp=os.path.join(out_folder_path,a_pic[i])
#cv2.imwrite(temp,gray_trim_crop)







cv2.waitKey(0)
cv2.destroyAllWindows()

